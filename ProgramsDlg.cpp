// ProgramsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "ProgramsDlg.h"
#include "Usefull.h"
#include "CustomItems.h"

/*--------------------------------------------------------------------
	Count how many numbers a float contains
--------------------------------------------------------------------*/
int CountFraction(float fNumber)
{
	CString csBuf;
	csBuf.Format(_T("%g"), fNumber);

	int nFound = csBuf.FindOneOf(_T(",."));
	if(nFound != -1)
		return csBuf.GetLength() - nFound - 1;
	else
		return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDoc

IMPLEMENT_DYNCREATE(CProgramsDoc, CDocument)

BEGIN_MESSAGE_MAP(CProgramsDoc, CDocument)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramsDoc construction/destruction

CProgramsDoc::CProgramsDoc()
{
}

CProgramsDoc::~CProgramsDoc()
{
}

void CProgramsDoc::ConvertToHtml(CString *pcsData)
{
	pcsData->Replace(_T("&"), _T("&amp;"));
	pcsData->Replace(_T("<"), _T("&lt;"));
	pcsData->Replace(_T(">"), _T("&gt;"));
	pcsData->Replace(_T("\""), _T("&quot;"));
//	pcsData->Replace(_T("/"), _T("&#47;"));
}

/*--------------------------------------------------------------------
	Count how many numbers an integer contains
--------------------------------------------------------------------*/
int CProgramsDoc::CountInt(int nNumber)
{
	int nRet = 0;

	do
	{
		nNumber /= 10;
		nRet++;
	}
	while(nNumber != 0);
	
	return nRet;
}

BOOL CProgramsDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if(!SaveModified())
		return FALSE;

	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	return TRUE;
}

BOOL CProgramsDoc::OnNewDocument()
{
	if(!SaveModified())
		return FALSE;

	if (!CDocument::OnNewDocument())
		return FALSE;

	m_caHeaderVars.RemoveAll();
	m_caDataVars.RemoveAll();
	m_caTexts.RemoveAll();
	TEXT_TYPE text;


// variable type (0 = int, 1 = float, 2 = text, 3 = list, 4 = date, 5 = scale, 6 = vertex, 7 = gps, 8 = postex, 9 = DigiTape, 10 = Boolean, 11 = calculation, 12 = DP DME, 13 = DP DME Postex, 14 = DP DME GPS)

	// program-text
	text.csName = g_pXML->str(8); //_T("Measure");
	text.csCaption = g_pXML->str(8); //_T("Measure");
	m_caTexts.Add(text);
	
	text.csName = g_pXML->str(9); //_T("Print");
	text.csCaption = g_pXML->str(9); //_T("Print");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(10); //_T("Delete lists");
	text.csCaption = g_pXML->str(10); //_T("Delete lists");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(11); //_T("Settings");
	text.csCaption = g_pXML->str(11); //_T("Settings");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(12); //_T("Shutdown");
	text.csCaption = g_pXML->str(12); //_T("Shutdown");
	m_caTexts.Add(text);

	// measure
	text.csName = g_pXML->str(13); //_T("Measure");
	text.csCaption = g_pXML->str(13); //_T("Measure");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(14); //_T("List ID");
	text.csCaption = g_pXML->str(14); //_T("List ID");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(15); //_T("Print");
	text.csCaption = g_pXML->str(15); //_T("Print");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(16); //_T("Delete data");
	text.csCaption = g_pXML->str(16); //_T("Delete data");
	m_caTexts.Add(text);

	// settings
	text.csName = g_pXML->str(17); //_T("System");
	text.csCaption = g_pXML->str(17); //_T("System");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(18); //_T("Reset");
	text.csCaption = g_pXML->str(18); //_T("Reset");
	m_caTexts.Add(text);


	// various text
	text.csName = g_pXML->str(19); //_T("Printing...");
	text.csCaption = g_pXML->str(19); //_T("Printing...");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(20); //_T("Done!");
	text.csCaption = g_pXML->str(20); //_T("Done!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(21); //_T("Can not open port!");
	text.csCaption = g_pXML->str(21); //_T("Can not open port!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(22); //_T("New list?");
	text.csCaption = g_pXML->str(22); //_T("New list?");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(23); //_T("Delete?");
	text.csCaption = g_pXML->str(23); //_T("Delete?");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(24); //_T("Unable to delete file!");
	text.csCaption = g_pXML->str(24); //_T("Unable to delete file!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(25); //_T("ERROR!");
	text.csCaption = g_pXML->str(25); //_T("ERROR!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(26); //_T("File exist!");
	text.csCaption = g_pXML->str(26); //_T("File exist!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(27); //_T("Can't open file!");
	text.csCaption = g_pXML->str(27); //_T("Can't open file!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(28); //_T("Write eror!");
	text.csCaption = g_pXML->str(28); //_T("Write error!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(29); //_T("Read error!");
	text.csCaption = g_pXML->str(29); //_T("Read error!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(30); //_T("Wrong version of file!");
	text.csCaption = g_pXML->str(30); //_T("Wrong version of file!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(80); //_T("No tape found!");
	text.csCaption = g_pXML->str(80); //_T("No tape found!");
	m_caTexts.Add(text);

	text.csName = g_pXML->str(91); // rectract wire
	text.csCaption = g_pXML->str(91); // retract wire
	m_caTexts.Add(text);

// 25

	text.csName = g_pXML->str(95); // yes
	text.csCaption = g_pXML->str(95); // yes
	m_caTexts.Add(text);

	text.csName = g_pXML->str(96); // no
	text.csCaption = g_pXML->str(96); // no
	m_caTexts.Add(text);
// 27
	text.csName = g_pXML->str(114); // value to big!
	text.csCaption = g_pXML->str(114); // value to big!
	m_caTexts.Add(text);

	text.csName = g_pXML->str(115); // value to small!
	text.csCaption = g_pXML->str(115); // value to small!
	m_caTexts.Add(text);

	return TRUE;
}

/************************************************************************/
/* Opens or saves a file in the Vbx format.                             */
/*                                                                      */
/* @param aOpenFile - TRUE to use the open file dialog and              */ 
/* FALSE(default) if the save dialog should be used.                    */
/*                                                                      */
/* @returns BOOL - Returns if the operation is successful to either open*/ 
/* the file or save it. FALSE if the user cancel the dialog or the      */
/* operation has failed.                                                */
/************************************************************************/
BOOL CProgramsDoc::VbxFileDialog(BOOL aOpenFile)
{
	// show a filechooser
	TCHAR tzFilename[1024], tzTitle[50];

	if(aOpenFile)
	{ //open vbx file
		_stprintf(tzFilename, _T("*.vbx"));
		_stprintf(tzTitle, g_pXML->str(62));	// open xml

		CFileDialog dlgF(TRUE);
		dlgF.m_ofn.lpstrTitle = tzTitle;
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("VBX (*.vbx)\0*.vbx\0\0");
		dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
		dlgF.m_ofn.lpstrFile = tzFilename;
		
		if(dlgF.DoModal() == IDOK)
		{
			return (OnOpenDocument(dlgF.GetPathName()) != 0);
		}
	}
	else
	{ //save vbx file
		_stprintf(tzFilename, _T(""));
		_stprintf(tzTitle, g_pXML->str(61)); //_T("Save XML"));	//g_pXML->str(1064));

		if(GetPathName() != _T(""))	// old file
		{
			_stprintf(tzFilename, GetPathName());
		}

		CFileDialog dlgF(FALSE);
		dlgF.m_ofn.lpstrTitle = tzTitle;
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("VBX (*.vbx)\0*.vbx\0\0");
		dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
		dlgF.m_ofn.lpstrFile = tzFilename;
		dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;
		dlgF.m_ofn.lpstrDefExt = _T(".vbx");

		if(dlgF.DoModal() == IDOK)
		{
			return (OnSaveDocument(dlgF.GetPathName()) != 0);
		}
	}
	return FALSE;
}

BOOL CProgramsDoc::SaveModified()
{
	if(IsModified())
	{
		POSITION pos = GetFirstViewPosition();
		CView* pFirstView = GetNextView( pos );

		int nRet = ::MessageBox(pFirstView->m_hWnd, g_pXML->str(60), g_pXML->str(59), MB_YESNOCANCEL|MB_ICONQUESTION); // "Project is changed, save changes?", "Save"
		if(nRet == IDCANCEL) return 0;	
		else if(nRet == IDYES)
		{
			if(!theApp.CheckLicense()) return FALSE;
		
			return VbxFileDialog();
		}
	}

	return 1;	// close
}

void CProgramsDoc::OnFileSave()
{
	// save
	OnFileSave();
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDoc serialization

void CProgramsDoc::Serialize(CArchive& ar)
{
	CString csFilename, csBuf;
	csFilename = ar.GetFile()->GetFilePath();
	VAR_TYPE var;
	TEXT_TYPE text;
	
	if (ar.IsStoring())
	{
		// check if we have a valid license.
		if(!theApp.CheckLicense())
		{
			AfxMessageBox(g_pXML->str(75));
			ar.Abort();
			return;
		}

		// check for errors amongst variables
		CString csError;
		BOOL bError = FALSE;
		int nLoop;

		for(nLoop=0; nLoop<m_caHeaderVars.GetSize(); nLoop++)
		{
			var = m_caHeaderVars.GetAt(nLoop);

			if(var.nType == 0)	// int
			{
				if(var.nMin > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(100));
					csError += csBuf;
				}
				if(var.nStep > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(101));
					csError += csBuf;
				}
				if(var.nMin == var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(102));
					csError += csBuf;
				}
				if((var.fMax-var.nMin) >= 4294967296)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(104));
					csError += csBuf;
				}
			}
			else if(var.nType == 1)	// float
			{
				if(var.nMin > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(100));
					csError += csBuf;
				}
				if(var.nMin == var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(102));
					csError += csBuf;
				}
			}
			else if(var.nType == 2)	// text
			{
				if(var.nMaxLength > 50)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(105));
					csError += csBuf;
				}
			}
		}		

		for(nLoop=0; nLoop<m_caDataVars.GetSize(); nLoop++)
		{
			var = m_caDataVars.GetAt(nLoop);

			if(var.nType == 0)	// int
			{
				if(var.nMin > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(100));
					csError += csBuf;
				}
				if(var.nStep > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(101));
					csError += csBuf;
				}
				if(var.nMin == var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(102));
					csError += csBuf;
				}
				if((var.fMax-var.nMin) >= 4294967296)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(104));
					csError += csBuf;
				}
			}
			else if(var.nType == 1)	// float
			{
				if(var.nMin > var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(100));
					csError += csBuf;
				}
				if(var.nMin == var.fMax)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(102));
					csError += csBuf;
				}
			}
			else if(var.nType == 2)	// text
			{
				if(var.nMaxLength > 50)
				{
					bError = TRUE;
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(105));
					csError += csBuf;
				}
			}
		}		

		if(bError)
		{
			AfxMessageBox(csError);
			ar.Abort();
			return;
		}


		char szBuf[1024];
		int nAmount;
		CString csBuf = "";

		// generate the code
		unsigned long nChecksum = 0;
		USES_CONVERSION;
		nChecksum = 0;
		for(nLoop=0; nLoop<m_caHeaderVars.GetSize(); nLoop++)
		{
			var = m_caHeaderVars.GetAt(nLoop);
			sprintf(szBuf, "%S", var.csName);
			nChecksum += CalculateBlockCRC32(var.csName.Left(30).GetLength(), (unsigned char*)&szBuf);

			sprintf(szBuf, "%S", var.csCaption);
			nChecksum += CalculateBlockCRC32(var.csCaption.Left(20).GetLength(), (unsigned char*)&szBuf);
		}
		for(nLoop=0; nLoop<m_caDataVars.GetSize(); nLoop++)
		{
			var = m_caDataVars.GetAt(nLoop);
			sprintf(szBuf, "%S", var.csName);
			nChecksum += CalculateBlockCRC32(var.csName.Left(30).GetLength(), (unsigned char*)&szBuf);

			sprintf(szBuf, "%S", var.csCaption);
			nChecksum += CalculateBlockCRC32(var.csCaption.Left(20).GetLength(), (unsigned char*)&szBuf);
		}

		// save xml
		sprintf(szBuf, "<?xml version=\"1.0\"?>\r\n");
//		sprintf(szBuf, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		sprintf(szBuf, "<Setup>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		sprintf(szBuf, "\t<Id>%08X</Id>\r\n", nChecksum);
		ar.Write(szBuf, strlen(szBuf));

		sprintf(szBuf, "\t<HeaderVars>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		for(nLoop=0; nLoop<m_caHeaderVars.GetSize(); nLoop++)
		{
			var = m_caHeaderVars.GetAt(nLoop);

			csBuf = var.csList;
			nAmount = csBuf.Replace(_T(";"), _T(";"));
			if(nAmount > NUM_OF_LIST)
			{
				POSITION pos = GetFirstViewPosition();
				CView* pFirstView = GetNextView( pos );
				csBuf.Format(g_pXML->str(67), var.csName, NUM_OF_LIST);
				::MessageBox(pFirstView->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONEXCLAMATION); // "Variable %s has a list with more than %d items. It will be truncated.", "Warning"
			}

			ConvertToHtml(&var.csName);
			ConvertToHtml(&var.csCaption);
			ConvertToHtml(&var.csList);

			if(var.nType == 0)	// int
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Min=\"%d\" Max=\"%.0f\" Step=\"%d\" Scale=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMin, var.fMax, var.nStep, var.bScale);
			}
			else if(var.nType == 1)	// float
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Min=\"%d\" Max=\"%.*f\" Decimals=\"%d\" Scale=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMin, var.nDecimals, var.fMax, var.nDecimals, var.bScale);
			}
			else if(var.nType == 2)	// text
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" MaxLength=\"%d\" Scale=\"%d\" Bluetooth=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMaxLength, var.bScale, var.bBluetooth);
			}
			else if(var.nType == 3)	// list
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" List=\"%S\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.csList);
			}
			else if(var.nType == 4)	// date
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Scale=\"%d\" DateFormat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bScale, var.nDateformat);
			}
			else if(var.nType == 5)	// scale
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Factor=\"%f\" GatorEyes=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.fFactor, var.bGatorEyes);
			}
			else if(var.nType == 6)	// vertex
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Factor=\"%f\" Bluetooth=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.fFactor, var.bBluetooth);
			}
			else if(var.nType == 7)	// gps
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}
			else if(var.nType == 8)	// postex
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Bluetooth=\"%d\" Postex=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bBluetooth, var.nPostex);
			}
			else if(var.nType == 9)	// digitape
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" DTMode=\"%d\" Factor=\"%f\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nDigiTapeMode, var.fFactor);
			}
			else if(var.nType == 10)	// bool
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}
			else if(var.nType == 11)	// calculation
			{
				VAR_TYPE varTmp;
				int nPos = 0;
				CString csToken = var.csParameters.Tokenize(_T(";"), nPos);
				CString csBuf, csTmp;
				while(nPos != -1)
				{
					for(int nLoop=0; nLoop<m_caHeaderVars.GetCount(); nLoop++)
					{
						varTmp = m_caHeaderVars.GetAt(nLoop);
						if(varTmp.csName == csToken)
						{
							csTmp.Format(_T("%d;"), nLoop);
							csBuf += csTmp;
							break;
						}
					}

					csToken = var.csParameters.Tokenize(_T(";"), nPos);
				};

				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Formula=\"%d\" Parameters=\"%S\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nFormula, csBuf);
			}
			else if(var.nType == 12)	// DPDME
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}
			else if(var.nType == 13)	// DP DME POSTEX
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Postex=\"%d\" PostexDia=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nPostex, var.bPostexDia);
			}
			else if(var.nType == 14)	// DP DME GPS
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}

			ar.Write(szBuf, strlen(szBuf));
		}

		sprintf(szBuf, "\t</HeaderVars>\r\n");
		ar.Write(szBuf, strlen(szBuf));


		sprintf(szBuf, "\t<DataVars>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		for(nLoop=0; nLoop<m_caDataVars.GetSize(); nLoop++)
		{
			var = m_caDataVars.GetAt(nLoop);

			csBuf = var.csList;
			nAmount = csBuf.Replace(_T(";"), _T(";"));
			if(nAmount > NUM_OF_LIST)
			{
				POSITION pos = GetFirstViewPosition();
				CView* pFirstView = GetNextView( pos );
				csBuf.Format(g_pXML->str(67), var.csName, NUM_OF_LIST);
				::MessageBox(pFirstView->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONEXCLAMATION); // "Variable %s has a list with more than %d items. It will be truncated.", "Warning"
			}

			ConvertToHtml(&var.csName);
			ConvertToHtml(&var.csCaption);
			ConvertToHtml(&var.csList);

			if(var.nType == 0)	// int
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Min=\"%d\" Max=\"%.0f\" Step=\"%d\" Scale=\"%d\" Counter=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMin, var.fMax, var.nStep, var.bScale, var.bCounter, var.bRepeat);
			}
			else if(var.nType == 1)	// float
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Min=\"%d\" Max=\"%.*f\" Decimals=\"%d\" Scale=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMin, var.nDecimals, var.fMax, var.nDecimals, var.bScale, var.bRepeat);
			}
			else if(var.nType == 2)	// text
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" MaxLength=\"%d\" Scale=\"%d\" Bluetooth=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nMaxLength, var.bScale, var.bBluetooth, var.bRepeat);
			}
			else if(var.nType == 3)	// list
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" List=\"%S\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.csList, var.bRepeat);
			}
			else if(var.nType == 4)	// date
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Scale=\"%d\" DateFormat=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bScale, var.nDateformat, var.bRepeat);
			}
			else if(var.nType == 5)	// scale
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Factor=\"%f\" GatorEyes=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.fFactor, var.bGatorEyes);
			}
			else if(var.nType == 6)	// vertex
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Factor=\"%f\" Repeat=\"%d\" Bluetooth=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.fFactor, var.bRepeat, var.bBluetooth);
			}
			else if(var.nType == 7)	// gps
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bRepeat);
			}
			else if(var.nType == 8)	// postex
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Repeat=\"%d\" Bluetooth=\"%d\" Postex=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bRepeat, var.bBluetooth, var.nPostex);
			}
			else if(var.nType == 9)	// digitape
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" DTMode=\"%d\" Factor=\"%f\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nDigiTapeMode, var.fFactor, var.bRepeat);
			}
			else if(var.nType == 10)	// bool
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Repeat=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bRepeat);
			}
			else if(var.nType == 11)	// calculation
			{
				VAR_TYPE varTmp;
				int nPos = 0;
				CString csToken = var.csParameters.Tokenize(_T(";"), nPos);
				CString csBuf, csTmp;
				while(nPos != -1)
				{
					for(int nLoop=0; nLoop<m_caDataVars.GetCount(); nLoop++)
					{
						varTmp = m_caDataVars.GetAt(nLoop);
						if(varTmp.csName == csToken)
						{
							csTmp.Format(_T("%d;"), nLoop);
							csBuf += csTmp;
							break;
						}
					}

					csToken = var.csParameters.Tokenize(_T(";"), nPos);
				};

				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Formula=\"%d\" Parameters=\"%S\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.nFormula, csBuf);
			}
			else if(var.nType == 12)	// DPDME
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}
			else if(var.nType == 13)	// DPDME POSTEX
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\" Repeat=\"%d\" Postex=\"%d\" PostexDia=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType, var.bRepeat, var.nPostex, var.bPostexDia);
			}
			else if(var.nType == 14)	// DP DME GPS
			{
				sprintf(szBuf, "\t\t<Item Name=\"%.30S\" Caption=\"%.20S\" Type=\"%d\"/>\r\n",
					var.csName, var.csCaption, var.nType);
			}

			ar.Write(szBuf, strlen(szBuf));
		}

		sprintf(szBuf, "\t</DataVars>\r\n");
		ar.Write(szBuf, strlen(szBuf));


		sprintf(szBuf, "\t<Text>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		for(nLoop=0; nLoop<m_caTexts.GetSize(); nLoop++)
		{
			text = m_caTexts.GetAt(nLoop);

			ConvertToHtml(&text.csCaption);
			ConvertToHtml(&text.csName);

			sprintf(szBuf, "\t\t<Item Name=\"%S\" Caption=\"%S\"/>\r\n", 
				text.csName, text.csCaption);
			ar.Write(szBuf, strlen(szBuf));
		}

		sprintf(szBuf, "\t</Text>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		sprintf(szBuf, "</Setup>\r\n");
		ar.Write(szBuf, strlen(szBuf));

		SetPathName(csFilename);
	}
	else
	{
		// TODO: add loading code here
		XMLNode xNode1 = XMLNode::openFileHelper(csFilename, _T("XML"));
		XMLNode xNode = xNode1.getChildNode(_T("Setup"));

		if(xNode.isEmpty() != 1)
		{
			int nTexts = m_caTexts.GetSize();

			m_caHeaderVars.RemoveAll();
			m_caDataVars.RemoveAll();
			m_caTexts.RemoveAll();

			XMLNode xChild, xItem;
			int i, iterator=0;

			xChild = xNode.getChildNode(_T("HeaderVars"));
			int n = xChild.nChildNode(_T("Item"));
			for(i=0; i<n; i++)
			{
				var.csList = _T("");
				var.nDecimals = 0;
				var.fFactor = 0.0;
				var.fMax = 0.0;
				var.nStep = 0;
				var.nMaxLength = 5;
				var.nMin = 0;
				var.nType = 2;	// text
				var.bScale = FALSE;
				var.bCounter = FALSE;
				var.bBluetooth = FALSE;
				var.nDigiTapeMode = 1;	// 0 = diameter, 1 = length
				var.bGatorEyes = FALSE;
				var.nDateformat = 0;	// YYYY/MM/DD
				var.bPostexDia = FALSE;
				var.nPostex = 0;

				xItem = xChild.getChildNode(_T("Item"), &iterator);

				csBuf = xItem.getAttribute(_T("name"));
				if(csBuf != _T("")) var.csName = csBuf;

				csBuf = xItem.getAttribute(_T("caption"));
				if(csBuf != _T("")) var.csCaption = csBuf;

				csBuf = xItem.getAttribute(_T("type"));
				if(csBuf != _T("")) var.nType = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("min"));
				if(csBuf != _T("")) var.nMin = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("max"));
				if(csBuf != _T("")) var.fMax = _tstof(CLocale::FixLocale(csBuf));

				csBuf = xItem.getAttribute(_T("step"));
				if(csBuf != _T("")) var.nStep = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("decimals"));
				if(csBuf != _T("")) var.nDecimals = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("maxlength"));
				if(csBuf != _T("")) var.nMaxLength = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("factor"));
				if(csBuf != _T("")) var.fFactor = _tstof(CLocale::FixLocale(csBuf));

				csBuf = xItem.getAttribute(_T("list"));
				if(csBuf != _T("")) var.csList = csBuf;

				csBuf = xItem.getAttribute(_T("scale"));
				if(csBuf != _T("")) var.bScale = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("bluetooth"));
				if(csBuf != _T("")) var.bBluetooth = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("dtmode"));
				if(csBuf != _T("")) var.nDigiTapeMode = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("gatoreyes"));
				if(csBuf != _T("")) var.bGatorEyes = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("dateformat"));
				if(csBuf != _T("")) var.nDateformat = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("formula"));
				if(csBuf != _T("")) var.nFormula = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("parameters"));
				if(csBuf != _T(""))
				{
					VAR_TYPE varTmp;
					int nPos = 0;
					CString csToken = csBuf.Tokenize(_T(";"), nPos);
					CString csTmp;
					while(nPos != -1)
					{
						varTmp = m_caHeaderVars.GetAt( _tstoi(csToken) );
						csTmp = csTmp + varTmp.csName + _T(";");

						csToken = csBuf.Tokenize(_T(";"), nPos);
					};

					var.csParameters = csTmp;
				}

				csBuf = xItem.getAttribute(_T("postex"));
				if(csBuf != _T("")) var.nPostex = _tstoi(csBuf);


				m_caHeaderVars.Add(var);
			}

			iterator = 0;
			xChild = xNode.getChildNode(_T("DataVars"));
			n = xChild.nChildNode(_T("Item"));
			for(i=0; i<n; i++)
			{
				var.csList = _T("");
				var.nDecimals = 0;
				var.fFactor = 0.0;
				var.fMax = 0.0;
				var.nStep = 0;
				var.nMaxLength = 5;
				var.nMin = 0;
				var.nType = 2;	// text
				var.bScale = FALSE;
				var.bCounter = FALSE;
				var.bBluetooth = FALSE;
				var.nDigiTapeMode = 1;	// 0 = diameter, 1 = length
				var.bGatorEyes = FALSE;
				var.nDateformat = 0;	// YYYY/MM/DD
				var.bRepeat = FALSE;
				var.bPostexDia = FALSE;
				var.nPostex = 0;

				xItem = xChild.getChildNode(_T("Item"), &iterator);

				csBuf = xItem.getAttribute(_T("name"));
				if(csBuf != _T("")) var.csName = csBuf;

				csBuf = xItem.getAttribute(_T("caption"));
				if(csBuf != _T("")) var.csCaption = csBuf;

				csBuf = xItem.getAttribute(_T("type"));
				if(csBuf != _T("")) var.nType = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("min"));
				if(csBuf != _T("")) var.nMin = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("max"));
				if(csBuf != _T("")) var.fMax = _tstof(CLocale::FixLocale(csBuf));

				csBuf = xItem.getAttribute(_T("step"));
				if(csBuf != _T("")) var.nStep = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("decimals"));
				if(csBuf != _T("")) var.nDecimals = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("maxlength"));
				if(csBuf != _T("")) var.nMaxLength = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("factor"));
				if(csBuf != _T("")) var.fFactor = _tstof(CLocale::FixLocale(csBuf));

				csBuf = xItem.getAttribute(_T("list"));
				if(csBuf != _T("")) var.csList = csBuf;

				csBuf = xItem.getAttribute(_T("scale"));
				if(csBuf != _T("")) var.bScale = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("counter"));
				if(csBuf != _T("")) var.bCounter = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("dtmode"));
				if(csBuf != _T("")) var.nDigiTapeMode = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("bluetooth"));
				if(csBuf != _T("")) var.bBluetooth = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("gatoreyes"));
				if(csBuf != _T("")) var.bGatorEyes = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("dateformat"));
				if(csBuf != _T("")) var.nDateformat = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("formula"));
				if(csBuf != _T("")) var.nFormula = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("parameters"));
				if(csBuf != _T(""))
				{
					VAR_TYPE varTmp;
					int nPos = 0;
					CString csToken = csBuf.Tokenize(_T(";"), nPos);
					CString csTmp;
					while(nPos != -1)
					{
						varTmp = m_caDataVars.GetAt( _tstoi(csToken) );
						csTmp = csTmp + varTmp.csName + _T(";");

						csToken = csBuf.Tokenize(_T(";"), nPos);
					};

					var.csParameters = csTmp;
				}

				csBuf = xItem.getAttribute(_T("repeat"));
				if(csBuf != _T("")) var.bRepeat = _tstoi(csBuf);

				csBuf = xItem.getAttribute(_T("postex"));
				if(csBuf != _T("")) var.nPostex = _tstoi(csBuf);

				m_caDataVars.Add(var);
			}

			iterator = 0;
			xChild = xNode.getChildNode(_T("Text"));
			n = xChild.nChildNode(_T("Item"));
			for(i=0; i<n; i++)
			{
				xItem = xChild.getChildNode(_T("Item"), &iterator);

				csBuf = xItem.getAttribute(_T("name"));
				if(csBuf != _T("")) text.csName = csBuf;

				csBuf = xItem.getAttribute(_T("caption"));
				if(csBuf != _T("")) text.csCaption = csBuf;

				m_caTexts.Add(text);
			}

			if(nTexts > n)
			{
				if(n < 27)
				{
					text.csName = g_pXML->str(95); // yes
					text.csCaption = g_pXML->str(95); // yes
					m_caTexts.Add(text);

					text.csName = g_pXML->str(96); // no
					text.csCaption = g_pXML->str(96); // no
					m_caTexts.Add(text);
				}

				if(n < 29)
				{
					text.csName = g_pXML->str(114); // value to big!
					text.csCaption = g_pXML->str(114); // value to big!
					m_caTexts.Add(text);

					text.csName = g_pXML->str(115); // value to small!
					text.csCaption = g_pXML->str(115); // value to small!
					m_caTexts.Add(text);
				}
			}

			SetPathName(csFilename);
		}
		else	// an error occured?
		{
			SetModifiedFlag(FALSE);
			AfxThrowArchiveException(CArchiveException::badIndex, csFilename);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDoc diagnostics

#ifdef _DEBUG
void CProgramsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CProgramsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsDoc commands





IMPLEMENT_DYNCREATE(CProgramsFrame, CMDIChildWnd)
BEGIN_MESSAGE_MAP(CProgramsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProgramsFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
};

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame construction/destruction

CProgramsFrame::CProgramsFrame()
{
	m_bOnce = TRUE;
}

CProgramsFrame::~CProgramsFrame()
{
}

LRESULT CProgramsFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
			((CProgramsDlg*)GetActiveView())->OnNew();
		break;

		case ID_OPEN_ITEM:
			((CProgramsDlg*)GetActiveView())->OnOpen();
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
			((CProgramsDlg*)GetActiveView())->OnSave();
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CProgramsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_DP Generator\\Dialogs\\Programs"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CProgramsFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
}

void CProgramsFrame::OnSetFocus(CWnd* pOldWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);
}

void CProgramsFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_DP Generator\\Dialogs\\Programs"), REG_ROOT);
	SavePlacement(this, csBuf);

	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

BOOL CProgramsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CProgramsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame diagnostics

#ifdef _DEBUG
void CProgramsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CProgramsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame message handlers

int CProgramsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// ugliness(tm) because of being loaded as a module
	CString csLangFN;
	csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	if (!g_pXML->Load(csLangFN))
	{
		AfxMessageBox(_T("Versio.dll: Could not open languagefile!"), MB_ICONERROR);
		AfxMessageBox(csLangFN);
	}


	// Create and Load toolbar; 051219 påd
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_VERSIO);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 påd
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 påd
				if (nBarID == IDR_TOOLBAR_VERSIO)
				{		
						// add variable
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(63));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 0);	// add
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_ADD);

						// delete variable
						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(64));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 28);	// minus
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_DEL);

						// move variable up
						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(65));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 62);	// u
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_UP);

						// move variable down
						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(g_pXML->str(66));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 61);	// d
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_DOWN);

						// send program to caliper
						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip(g_pXML->str(69));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 5);	// comto
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_SEND);
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	UpdateWindow();

	return 0;
}

void CProgramsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

void CProgramsFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

/*---------------------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg

#define TYPE_HEADER 2
#define TYPE_DATA	3
#define TYPE_TEXT	4

IMPLEMENT_DYNCREATE(CProgramsDlg, CXTResizeFormView)

void CProgramsDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_TREE, m_TreeCtrl);
	DDX_Control(pDX, IDC_PROPERTIES, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CProgramsDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramsDlg)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(ID_BUTTON_ADD, OnBnClickedAdd)
	ON_BN_CLICKED(ID_BUTTON_DEL, OnBnClickedDel)
	ON_BN_CLICKED(ID_BUTTON_UP, OnBnClickedUp)
	ON_BN_CLICKED(ID_BUTTON_DOWN, OnBnClickedDown)
	ON_BN_CLICKED(ID_BUTTON_SEND, OnBnClickedSend)
	ON_NOTIFY(NM_RCLICK, IDC_TREE, OnRclickTreeCtrl)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE, OnTvnSelchangedTree)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg construction/destruction

CProgramsDlg::CProgramsDlg()
	: CXTResizeFormView(CProgramsDlg::IDD)
{
	m_bGotDoc = FALSE;
}

CProgramsDlg::~CProgramsDlg()
{
}

BOOL CProgramsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CView::PreCreateWindow(cs))
		return FALSE;

	return TRUE;

}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg diagnostics

#ifdef _DEBUG
void CProgramsDlg::AssertValid() const
{
	CView::AssertValid();
}

void CProgramsDlg::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
/*
CDocument* CProgramsDlg::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDocument)));
	return (CDocument*)m_pDocument;
}*/
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg message handlers

int CProgramsDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CProgramsDlg::OnDestroy()
{
	CView::OnDestroy();
}

void CProgramsDlg::OnSetFocus(CWnd* pOldWnd)
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, m_bGotDoc);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

	CXTResizeFormView::OnSetFocus(pOldWnd);
}

/*---------------------------------------------------------------
	Initialize the view and all its controls
---------------------------------------------------------------*/
void CProgramsDlg::OnInitialUpdate()
{
	// get a handle to the document
	pDoc = (CProgramsDoc*)GetDocument();

	CView::OnInitialUpdate();
	m_nCurIndex = 0;


	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );


	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);
		m_wndPropertyGrid.EnableWindow(FALSE);

		CXTPPropertyGridItemConstraints* pList;

		// Header-variable
		m_pHeader = m_wndPropertyGrid.AddCategory(g_pXML->str(31)); //_T("Header-variable"));
		m_pHeaderName = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(34), _T("")));	// name
		m_pHeaderName->SetMask(_T("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), _T("______________________________"), '_');
		m_pHeaderName->SetDescription(g_pXML->str(35));
		m_pHeaderCaption = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(36), _T("")));	// caption
		m_pHeaderCaption->SetMask(_T("aaaaaaaaaaaaaaaaaaaa"), _T("____________________"), '_');
		m_pHeaderCaption->SetDescription(g_pXML->str(37));
		m_pHeaderType = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(38), _T("")));	// type
		m_pHeaderType->SetDescription(g_pXML->str(39));
		pList = m_pHeaderType->GetConstraints();
		pList->AddConstraint(g_pXML->str(52), 0);	// numeric
		pList->AddConstraint(g_pXML->str(53), 1);	// decimal
		pList->AddConstraint(g_pXML->str(54), 2);	// text
		pList->AddConstraint(g_pXML->str(55), 3);	// list
		pList->AddConstraint(g_pXML->str(56), 4);	// date
		pList->AddConstraint(g_pXML->str(57), 5);	// scale
		pList->AddConstraint(g_pXML->str(58), 6);	// vertex
		pList->AddConstraint(g_pXML->str(74), 7);	// gps
		pList->AddConstraint(g_pXML->str(78), 8);	// postex
		pList->AddConstraint(g_pXML->str(79), 9);	// digitech tape
		pList->AddConstraint(g_pXML->str(92), 10);	// bool
//		pList->AddConstraint(g_pXML->str(94));	// calculation
		pList->AddConstraint(g_pXML->str(119), 12);	// DP DME
		pList->AddConstraint(g_pXML->str(120), 13);	// DP DME Postex
		pList->AddConstraint(g_pXML->str(127), 14);	// DP DME GPS
		m_pHeaderType->SetFlags(xtpGridItemHasComboButton);

		m_pHeaderMin = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(40), _T("")));	// min
		m_pHeaderMin->SetDescription(g_pXML->str(41));
		m_pHeaderMax = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(42), _T("")));	// max
		m_pHeaderMax->SetDescription(g_pXML->str(43));
		m_pHeaderStep = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(70), _T("")));	// step
		m_pHeaderStep->SetDescription(g_pXML->str(71));
		m_pHeaderDecimals = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(44), _T("")));	// decimals
		m_pHeaderDecimals->SetDescription(g_pXML->str(45));
		m_pHeaderMaxLength = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(46), _T("")));	// max length
		m_pHeaderMaxLength->SetDescription(g_pXML->str(47));
		m_pHeaderFactor = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(48), _T("")));	// factor
		m_pHeaderFactor->SetDescription(g_pXML->str(49));
		m_pHeaderList = m_pHeader->AddChildItem(new CCustomItemButton(g_pXML->str(50), _T("")));
		m_pHeaderList->SetReadOnly();
		m_pHeaderList->SetDescription(g_pXML->str(51));
		m_pHeaderScale = (CXTPPropertyGridItemBool*)m_pHeader->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(72), FALSE));
		m_pHeaderScale->SetDescription(g_pXML->str(73));
		m_pHeaderBluetooth = (CXTPPropertyGridItemBool*)m_pHeader->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(83), FALSE));
		m_pHeaderBluetooth->SetDescription(g_pXML->str(84));
		m_pHeaderDigiTapeDia = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(85), _T("")));	// Mode: diameter or length
		m_pHeaderDigiTapeDia->SetDescription(g_pXML->str(86));
		pList = m_pHeaderDigiTapeDia->GetConstraints();
		pList->AddConstraint(g_pXML->str(87));	// diameter
		pList->AddConstraint(g_pXML->str(88));	// length
		m_pHeaderDigiTapeDia->SetFlags(xtpGridItemHasComboButton);
		m_pHeaderGatorEyes = (CXTPPropertyGridItemBool*)m_pHeader->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(89), FALSE));
		m_pHeaderGatorEyes->SetDescription(g_pXML->str(90));
		m_pHeaderDateformat = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(110), _T("")));	// Date format
		m_pHeaderDateformat->SetDescription(g_pXML->str(111));	//_T("Set the date format to be used."));
		pList = m_pHeaderDateformat->GetConstraints();
		pList->AddConstraint(g_pXML->str(112));	// YYYY/MM/DD
		pList->AddConstraint(g_pXML->str(113));	// DD/MM/YYYY
		m_pHeaderDateformat->SetFlags(xtpGridItemHasComboButton);
		m_pHeaderCalculation = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(97), _T("")));	// calculation
		m_pHeaderCalculation->SetDescription(g_pXML->str(98));
		pList = m_pHeaderCalculation->GetConstraints();
		pList->AddConstraint(g_pXML->str(99));	// mean value
		m_pHeaderCalculation->SetFlags(xtpGridItemHasComboButton);
		m_pHeaderParameterList = m_pHeader->AddChildItem(new CCustomItemButton(g_pXML->str(108), _T(""), TRUE));	// parameters
		m_pHeaderParameterList->SetReadOnly();
		m_pHeaderParameterList->SetDescription(g_pXML->str(109));
		m_pHeaderPostex = m_pHeader->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(121), _T("")));	// Postex mode
		m_pHeaderPostex->SetDescription(g_pXML->str(122));
		pList = m_pHeaderPostex->GetConstraints();
		pList->AddConstraint(g_pXML->str(123));	// x&y
		pList->AddConstraint(g_pXML->str(124));	// deg&dist (360)
		pList->AddConstraint(g_pXML->str(125));	// deg&dist (400)
		//pList->AddConstraint(g_pXML->str(126));	// d1,d2,d3
		m_pHeaderPostex->SetFlags(xtpGridItemHasComboButton);
		m_pHeaderPostexDia = (CXTPPropertyGridItemBool*)m_pHeader->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(128), FALSE));
		m_pHeaderPostexDia->SetDescription(g_pXML->str(129));

		m_pHeader->Expand();
		m_pHeader->SetHidden(TRUE);


		// Data-variable
		m_pData = m_wndPropertyGrid.AddCategory(g_pXML->str(32));	// data variable
		m_pDataName = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(34), _T("")));	// name
		m_pDataName->SetMask(_T("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), _T("______________________________"), '_');
		m_pDataName->SetDescription(g_pXML->str(35));
		m_pDataCaption = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(36), _T("")));	// caption
		m_pDataCaption->SetMask(_T("aaaaa"), _T("_____"), '_');
		m_pDataCaption->SetDescription(g_pXML->str(37));
		m_pDataType = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(38), _T("")));	// type
		m_pDataType->SetDescription(g_pXML->str(39));
		pList = m_pDataType->GetConstraints();
		pList->AddConstraint(g_pXML->str(52), 0);	// numeric
		pList->AddConstraint(g_pXML->str(53), 1);	// decimal
		pList->AddConstraint(g_pXML->str(54), 2);	// text
		pList->AddConstraint(g_pXML->str(55), 3);	// list
		pList->AddConstraint(g_pXML->str(56), 4);	// date
		pList->AddConstraint(g_pXML->str(57), 5);	// scale
		pList->AddConstraint(g_pXML->str(58), 6);	// vertex
		pList->AddConstraint(g_pXML->str(74), 7);	// gps
		pList->AddConstraint(g_pXML->str(78), 8);	// postex
		pList->AddConstraint(g_pXML->str(79), 9);	// digitech tape
		pList->AddConstraint(g_pXML->str(92), 10);	// bool
		pList->AddConstraint(g_pXML->str(94), 11);	// calculation
		pList->AddConstraint(g_pXML->str(119), 12);	// DP DME
		pList->AddConstraint(g_pXML->str(120), 13);	// DP DME Postex
		pList->AddConstraint(g_pXML->str(127), 14);	// DP DME GPS
		m_pDataType->SetFlags(xtpGridItemHasComboButton);

		m_pDataMin = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(40), _T("")));	// min
		m_pDataMin->SetDescription(g_pXML->str(41));
		m_pDataMax = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(42), _T("")));	// max
		m_pDataMax->SetDescription(g_pXML->str(43));
		m_pDataStep = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(70), _T("")));	// step
		m_pDataStep->SetDescription(g_pXML->str(71));
		m_pDataDecimals = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(44), _T("")));	// decimals
		m_pDataDecimals->SetDescription(g_pXML->str(45));
		m_pDataMaxLength = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(46), _T("")));	// max length
		m_pDataMaxLength->SetDescription(g_pXML->str(47));
		m_pDataFactor = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(48), _T("")));	// factor
		m_pDataFactor->SetDescription(g_pXML->str(49));
		m_pDataList = m_pData->AddChildItem(new CCustomItemButton(g_pXML->str(50), _T("")));	// list
		m_pDataList->SetReadOnly();
		m_pDataList->SetDescription(g_pXML->str(51));
		m_pDataScale = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(72), FALSE));	// scale
		m_pDataScale->SetDescription(g_pXML->str(73));
		m_pDataCounter = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(76), FALSE));	// counter
		m_pDataCounter->SetDescription(g_pXML->str(77));
		m_pDataBluetooth = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(83), FALSE));	// bluetooth
		m_pDataBluetooth->SetDescription(g_pXML->str(84));
		m_pDataDigiTapeDia = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(85), _T("")));	// Mode: diameter or length
		m_pDataDigiTapeDia->SetDescription(g_pXML->str(86));
		pList = m_pDataDigiTapeDia->GetConstraints();
		pList->AddConstraint(g_pXML->str(87));	// diameter
		pList->AddConstraint(g_pXML->str(88));	// length
		m_pDataDigiTapeDia->SetFlags(xtpGridItemHasComboButton);
		m_pDataGatorEyes = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(89), FALSE));	// bluetooth
		m_pDataGatorEyes->SetDescription(g_pXML->str(90));
		m_pDataDateformat = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(110), _T("")));	// Date format
		m_pDataDateformat->SetDescription(g_pXML->str(111));	//_T("Set the date format to be used."));
		pList = m_pDataDateformat->GetConstraints();
		pList->AddConstraint(g_pXML->str(112));	// YYYY/MM/DD
		pList->AddConstraint(g_pXML->str(113));	// DD/MM/YYYY
		m_pDataDateformat->SetFlags(xtpGridItemHasComboButton);
		m_pDataCalculation = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(97), _T("")));	// calculation
		m_pDataCalculation->SetDescription(g_pXML->str(98));
		pList = m_pDataCalculation->GetConstraints();
		pList->AddConstraint(g_pXML->str(99));	// mean value
		m_pDataCalculation->SetFlags(xtpGridItemHasComboButton);
		m_pDataParameterList = m_pData->AddChildItem(new CCustomItemButton(g_pXML->str(108), _T(""), TRUE));	// parameters
		m_pDataParameterList->SetReadOnly();
		m_pDataParameterList->SetDescription(g_pXML->str(109));
		m_pDataRepeat = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(118), FALSE));
		m_pDataRepeat->SetDescription(g_pXML->str(116));
		m_pDataPostex = m_pData->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(121), _T("")));	// Postex mode
		m_pDataPostex->SetDescription(g_pXML->str(122));
		pList = m_pDataPostex->GetConstraints();
		pList->AddConstraint(g_pXML->str(123));	// x&y
		pList->AddConstraint(g_pXML->str(124));	// deg&dist (360)
		pList->AddConstraint(g_pXML->str(125));	// deg&dist (400)
		//pList->AddConstraint(g_pXML->str(126));	// d1,d2,d3
		m_pDataPostex->SetFlags(xtpGridItemHasComboButton);
		m_pDataPostexDia = (CXTPPropertyGridItemBool*)m_pData->AddChildItem(new CMyPropGridItemBool(g_pXML->str(95), g_pXML->str(96), g_pXML->str(128), FALSE));
		m_pDataPostexDia->SetDescription(g_pXML->str(129));

		m_pData->Expand();
		m_pData->SetHidden(TRUE);


		// Text
		m_pText = m_wndPropertyGrid.AddCategory(g_pXML->str(3));	// text
		m_pTextName = m_pText->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(34), _T("")));	// name
		m_pTextName->SetDescription(g_pXML->str(35));
		m_pTextName->SetReadOnly(TRUE);
		m_pTextCaption = m_pText->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(36), _T("")));	// caption
		m_pTextCaption->SetDescription(g_pXML->str(37));
		m_pText->Expand();
		m_pText->SetHidden(TRUE);
	}



	CString sTBResFN = getProgDir() + _T("HMSToolBarIcons32.dll");
	HMODULE hResModule = LoadLibraryEx(sTBResFN, NULL, DONT_RESOLVE_DLL_REFERENCES|LOAD_LIBRARY_AS_DATAFILE);
	if (hResModule)
	{
		// setup the tree-control
		m_ImageList.Create(16, 16, ILC_COLOR32|ILC_MASK, 1, 1);
		m_ImageList.Add(LoadIcon(hResModule, _T("Treefolderclosed_16x16")));
		m_ImageList.Add(LoadIcon(hResModule, _T("Treefolderopen_16x16")));
		m_ImageList.Add(LoadIcon(hResModule, _T("Dp_header")));
		m_ImageList.Add(LoadIcon(hResModule, _T("Dp_data")));
		m_ImageList.Add(LoadIcon(hResModule, _T("Dp_text")));

		m_TreeCtrl.SetImageList(&m_ImageList, TVSIL_NORMAL);

		FreeLibrary(hResModule);
	}

	// Set control resizing.
	SetResize(IDC_TREE, SZ_TOP_LEFT, SZ_BOTTOM_CENTER);
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_CENTER, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	((CProgramsFrame*)GetParent())->SetButtonEnabled(0, m_bGotDoc);	// add
}

/*---------------------------------------------------------------
	Item selected in the tree
---------------------------------------------------------------*/
void CProgramsDlg::OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	CString csBuf;
	HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
	TVITEM item;
	TCHAR szText[1024];
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE;
	item.pszText = szText;
	item.iImage = 0;
	item.cchTextMax = 1024;
	m_TreeCtrl.GetItem(&item);


	// get the index of the chosen item
	int nIndex = 0;
	BOOL bFound = FALSE;
	VAR_TYPE var;
	TEXT_TYPE text;

	// user has selected a treeitem
	((CProgramsFrame*)GetParent())->SetButtonEnabled(0, FALSE);	// plus
	((CProgramsFrame*)GetParent())->SetButtonEnabled(1, FALSE);	// minus
	((CProgramsFrame*)GetParent())->SetButtonEnabled(2, FALSE);	// up
	((CProgramsFrame*)GetParent())->SetButtonEnabled(3, FALSE);	// down

	if(item.iImage == TYPE_TEXT)
	{
		// search for the text-name in the array
		for(int nLoop=0; nLoop<pDoc->m_caTexts.GetSize(); nLoop++)
		{
			text = pDoc->m_caTexts.GetAt(nLoop);
			if(text.csCaption == szText)
			{
				bFound = TRUE;
				break;
			}
		}
	}
	else if(item.iImage == TYPE_HEADER || item.iImage == TYPE_DATA)
	{
		// user has selected a treeitem
		((CProgramsFrame*)GetParent())->SetButtonEnabled(0, TRUE);	// plus

		if(_tcscmp(szText, g_pXML->str(1)) == 0 ||	// "header variables", "data variables"
			_tcscmp(szText, g_pXML->str(2)) == 0)
		{
			m_pHeader->SetHidden(TRUE);
			m_pData->SetHidden(TRUE);
			m_pText->SetHidden(TRUE);
			return;
		}

		((CProgramsFrame*)GetParent())->SetButtonEnabled(1, TRUE);	// minus

		while((hItem = m_TreeCtrl.GetPrevSiblingItem(hItem)) != NULL)
		{
			nIndex++;
		};

		if(nIndex != 0)
			((CProgramsFrame*)GetParent())->SetButtonEnabled(2, TRUE);	// up
		else
			((CProgramsFrame*)GetParent())->SetButtonEnabled(2, FALSE);	// up

		if( (item.iImage == TYPE_HEADER && nIndex != pDoc->m_caHeaderVars.GetSize()-1) ||
			(item.iImage == TYPE_DATA && nIndex != pDoc->m_caDataVars.GetSize()-1) )
			((CProgramsFrame*)GetParent())->SetButtonEnabled(3, TRUE);	// down
		else
			((CProgramsFrame*)GetParent())->SetButtonEnabled(3, FALSE);	// down

		m_nSelectedIndex = nIndex;
	}


	// show the right stuff in the properties-control
	m_pHeader->SetHidden(TRUE);
	m_pData->SetHidden(TRUE);
	m_pText->SetHidden(TRUE);

	hItem = m_TreeCtrl.GetFirstSelectedItem();
	if(item.iImage == TYPE_HEADER)	// header-variable
	{
		var = pDoc->m_caHeaderVars.GetAt(nIndex);

		m_pHeaderName->SetValue(var.csName);
		m_pHeaderCaption->SetValue(var.csCaption);
		CXTPPropertyGridItemConstraints* pList = m_pHeaderType->GetConstraints();
		m_pHeaderType->SetValue( pList->GetAt(pList->FindConstraint(var.nType)) );

		// disable all items
		ShowHeaders(FALSE);

		SetHeaderValues(&var);

		if(var.nType == 0)	// int
		{
			m_pHeaderMin->SetHidden(FALSE);
			m_pHeaderMax->SetHidden(FALSE);
			m_pHeaderStep->SetHidden(FALSE);
			m_pHeaderScale->SetHidden(FALSE);
		}
		else if(var.nType == 1)	// float
		{
			m_pHeaderMin->SetHidden(FALSE);
			m_pHeaderMax->SetHidden(FALSE);
			m_pHeaderDecimals->SetHidden(FALSE);
			m_pHeaderScale->SetHidden(FALSE);
		}
		else if(var.nType == 2)	// text
		{
			m_pHeaderMaxLength->SetHidden(FALSE);
			m_pHeaderScale->SetHidden(FALSE);
			m_pHeaderBluetooth->SetHidden(FALSE);
		}
		else if(var.nType == 3)	// list
		{
			m_pHeaderList->Expand();
			m_pHeaderList->SetHidden(FALSE);
		}
		else if(var.nType == 4)	// date
		{
			m_pHeaderScale->SetHidden(FALSE);
			m_pHeaderDateformat->SetHidden(FALSE);
		}
		else if(var.nType == 5)	// scale
		{
			m_pHeaderFactor->SetHidden(FALSE);
			m_pHeaderGatorEyes->SetHidden(FALSE);
		}
		else if(var.nType == 6)	// vertex
		{
			m_pHeaderFactor->SetHidden(FALSE);
			m_pHeaderBluetooth->SetHidden(FALSE);
		}
		else if(var.nType == 8)	// postex
		{
			m_pHeaderBluetooth->SetHidden(FALSE);
			m_pHeaderPostex->SetHidden(FALSE);
		}
		else if(var.nType == 9)	// digitape
		{
			m_pHeaderDigiTapeDia->SetHidden(FALSE);
			m_pHeaderFactor->SetHidden(FALSE);
		}
		else if(var.nType == 10)	// bool
		{
		}
		/*else if(var.nType == 11)	// calculation
		{
			m_pHeaderCalculation->SetHidden(FALSE);

			// update the variable lists for the parameters
			UpdateParameters(var.csName);

			m_pHeaderParameterList->Expand();
			m_pHeaderParameterList->SetHidden(FALSE);
		}*/
		else if(var.nType == 12)	// DP DME
		{
		}
		else if(var.nType == 13)	// DP DME Postex
		{
			m_pHeaderPostex->SetHidden(FALSE);
			m_pHeaderPostexDia->SetHidden(FALSE);
		}

		m_pHeader->SetHidden(FALSE);
	}
	else if(item.iImage == TYPE_DATA)	// data-variable
	{
		var = pDoc->m_caDataVars.GetAt(nIndex);

		m_pDataName->SetValue(var.csName);
		m_pDataCaption->SetValue(var.csCaption);
		CXTPPropertyGridItemConstraints* pList = m_pDataType->GetConstraints();
		m_pDataType->SetValue( pList->GetAt(pList->FindConstraint(var.nType)) );

		// disable all items
		ShowData(FALSE);

		SetDataValues(&var);

		int nCurPos = 0;
		m_caVars.RemoveAll();
		CString resToken = var.csList.Tokenize(_T(";"), nCurPos);
		while (nCurPos != -1)
		{
			m_caVars.Add(resToken);
			resToken = var.csList.Tokenize(_T(";"), nCurPos);
		};

		nCurPos = 0;
		m_caPars.RemoveAll();
		resToken = var.csParameters.Tokenize(_T(";"), nCurPos);
		while (nCurPos != -1)
		{
			m_caPars.Add(resToken);
			resToken = var.csParameters.Tokenize(_T(";"), nCurPos);
		};


		if(var.nType == 0)	// int
		{
			m_pDataMin->SetHidden(FALSE);
			m_pDataMax->SetHidden(FALSE);
			m_pDataStep->SetHidden(FALSE);
			m_pDataScale->SetHidden(FALSE);
			m_pDataCounter->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 1)	// float
		{
			m_pDataMin->SetHidden(FALSE);
			m_pDataMax->SetHidden(FALSE);
			m_pDataDecimals->SetHidden(FALSE);
			m_pDataScale->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 2)	// text
		{
			m_pDataMaxLength->SetHidden(FALSE);
			m_pDataScale->SetHidden(FALSE);
			m_pDataBluetooth->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 3)	// list
		{
			m_pDataList->Expand();
			m_pDataList->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 4)	// date
		{
			m_pDataScale->SetHidden(FALSE);
			m_pDataDateformat->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 5)	// scale
		{
			m_pDataFactor->SetHidden(FALSE);
			m_pDataGatorEyes->SetHidden(FALSE);
		}
		else if(var.nType == 6)	// vertex
		{
			m_pDataFactor->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
			m_pDataBluetooth->SetHidden(FALSE);
		}
		else if(var.nType == 8)	// postex
		{
			m_pDataRepeat->SetHidden(FALSE);
			m_pDataBluetooth->SetHidden(FALSE);
			m_pDataPostex->SetHidden(FALSE);
		}
		else if(var.nType == 9)	// digitape
		{
			m_pDataDigiTapeDia->SetHidden(FALSE);
			m_pDataFactor->SetHidden(FALSE);
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 10)	// bool
		{
			m_pDataRepeat->SetHidden(FALSE);
		}
		else if(var.nType == 11)	// calculation
		{
			m_pDataCalculation->SetHidden(FALSE);

			// update the variable lists for the parameters
			UpdateParameters(var.csName);

			m_pDataParameterList->SetHidden(FALSE);
		}
		else if(var.nType == 12)	// DP DME
		{
		}
		else if(var.nType == 13)	// DP DME Postex
		{
			m_pDataPostex->SetHidden(FALSE);
			m_pDataPostexDia->SetHidden(FALSE);
		}

		m_pData->SetHidden(FALSE);
	}
	else if(item.iImage == TYPE_TEXT)	// program-text
	{
		if(bFound == TRUE)
		{
			m_pTextName->SetValue(text.csName);
			m_pTextCaption->SetValue(text.csCaption);

			m_pText->SetHidden(FALSE);
		}
	}
}

void CProgramsDlg::OnRclickTreeCtrl(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	return; 

	CPoint point;
	GetCursorPos(&point);

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	CWnd* pWndPopupOwner = this;

	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y,
		pWndPopupOwner);

	*pResult = 0;
}

/*---------------------------------------------------------------
	Value changed in the properties-object
---------------------------------------------------------------*/
LRESULT CProgramsDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	VAR_TYPE var, varTmp;
	TEXT_TYPE text;
	CString csBuf;
	int nAmount;

	// get which variable we are changing
	if(nGridAction == XTP_PGN_SELECTION_CHANGED)
	{
		HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
		HTREEITEM hItemTmp = hItem;
		TVITEM item;
		TCHAR szText[1024];
		item.hItem = hItem;
		item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE;
		item.pszText = szText;
		item.iImage = -1;
		item.cchTextMax = 1024;
		m_TreeCtrl.GetItem(&item);

		// get the index of the chosen item
		BOOL bFound = FALSE;

		m_nCurIndex = 0;

		if(item.iImage == TYPE_TEXT)
		{
			// search for the text-name in the array
			for(int nLoop=0; nLoop<pDoc->m_caTexts.GetSize(); nLoop++)
			{
				text = pDoc->m_caTexts.GetAt(nLoop);
				if(text.csCaption == szText)
				{
					bFound = TRUE;
					break;
				}
				m_nCurIndex++;
			}
		}
		else
		{
			while((hItemTmp = m_TreeCtrl.GetPrevSiblingItem(hItemTmp)) != NULL)
			{
				m_nCurIndex++;
			};
		}

		m_itemCurr = item;
	}

	switch (nGridAction)
	{
		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
			HTREEITEM hItemTmp = hItem;
			TVITEM item;
			TCHAR szText[1024];
			item.hItem = hItem;
			item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE;
			item.pszText = szText;
			item.iImage = -1;
			item.cchTextMax = 1024;
			m_TreeCtrl.GetItem(&item);

			if(m_itemCurr.hItem == NULL) m_itemCurr = item;

		
			pDoc->SetModifiedFlag(TRUE);

			if(pItem == m_pHeaderName)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				csBuf = m_pHeaderName->GetValue();
				csBuf.TrimRight('_');
				if(csBuf.GetLength() > 30) csBuf = csBuf.Left(30);

				// change this name in every parameter-list aswell
				for(int nLoop=0; nLoop<pDoc->m_caHeaderVars.GetCount(); nLoop++)
				{
					varTmp = pDoc->m_caHeaderVars.GetAt(nLoop);
					if(varTmp.nType == 11)	// calculation
					{
						varTmp.csParameters.Replace(var.csName, csBuf);
						pDoc->m_caHeaderVars.SetAt(nLoop, varTmp);
					}
				}
				var.csName = csBuf;

				// change variable-name in the list aswell
				m_TreeCtrl.SetItemText(m_itemCurr.hItem, var.csName);

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);

				// update the calculating variables aswell
				UpdateParameters(var.csName);
			}
			else if(pItem == m_pHeaderCaption)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.csCaption = m_pHeaderCaption->GetValue();
				var.csCaption.TrimRight('_');
				if(var.csCaption.GetLength() > 20) var.csCaption = var.csCaption.Left(20);
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderType)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pHeaderType->GetConstraints();
				int nRet = pList->GetConstraintAt(pList->GetCurrent())->m_dwData;

				// disable all items
				ShowHeaders(FALSE);

				if(nRet == 0)	// int
				{
					m_pHeaderMin->SetHidden(FALSE);
					m_pHeaderMax->SetHidden(FALSE);
					m_pHeaderStep->SetHidden(FALSE);
					m_pHeaderScale->SetHidden(FALSE);
				}
				else if(nRet == 1)	// float
				{
					m_pHeaderMin->SetHidden(FALSE);
					m_pHeaderMax->SetHidden(FALSE);
					m_pHeaderDecimals->SetHidden(FALSE);
					m_pHeaderScale->SetHidden(FALSE);
				}
				else if(nRet == 2)	// text
				{
					m_pHeaderMaxLength->SetHidden(FALSE);
					m_pHeaderScale->SetHidden(FALSE);
					m_pHeaderBluetooth->SetHidden(FALSE);
				}
				else if(nRet == 3)	// list
				{
					m_pHeaderList->SetHidden(FALSE);
				}
				else if(nRet == 4)	// date
				{
					m_pHeaderScale->SetHidden(FALSE);
					m_pHeaderDateformat->SetHidden(FALSE);
				}
				else if(nRet == 5)	// scale
				{
					m_pHeaderFactor->SetHidden(FALSE);
					m_pHeaderGatorEyes->SetHidden(FALSE);
				}
				else if(nRet == 6)	// vertex
				{
					m_pHeaderFactor->SetHidden(FALSE);
					m_pHeaderBluetooth->SetHidden(FALSE);
				}
				else if(nRet == 8)	// postex
				{
					m_pHeaderBluetooth->SetHidden(FALSE);
					m_pHeaderPostex->SetHidden(FALSE);
				}
				else if(nRet == 9)	// digitape
				{
					m_pHeaderDigiTapeDia->SetHidden(FALSE);
					m_pHeaderFactor->SetHidden(FALSE);
				}
				else if(nRet == 10)	// bool
				{
				}
/*				else if(nRet == 11)	// calculation
				{
					// update the variable lists for the parameters
					UpdateParameters(var.csName);

					m_pHeaderCalculation->SetHidden(FALSE);
					m_pHeaderParameterList->SetHidden(FALSE);
				}*/
				else if(nRet == 12)	// DP DME
				{
				}
				else if(nRet == 13)	// DP DME Postex
				{
					m_pHeaderPostex->SetHidden(FALSE);
					m_pHeaderPostexDia->SetHidden(FALSE);
				}

				var.nType = nRet;
				SetHeaderValues(&var);
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderMin)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.nMin = _tstoi(m_pHeaderMin->GetValue());
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderMax)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.fMax = _tstof(m_pHeaderMax->GetValue());

				// count how many decimals fMax has and set that in nDecimals.
				var.nDecimals = CountFraction(var.fMax);
				csBuf.Format(_T("%d"), var.nDecimals);
				m_pHeaderDecimals->SetValue(csBuf);

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderStep)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.nStep = _tstoi(m_pHeaderStep->GetValue());
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderDecimals)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.nDecimals = _tstoi(m_pHeaderDecimals->GetValue());

				// add/remove decimals on fMax if needed
				if( CountFraction(var.fMax) != var.nDecimals )
				{
					csBuf.Format(_T("%.*f"), var.nDecimals, var.fMax);
					var.fMax = _tstof(CLocale::FixLocale(csBuf));
					m_pHeaderMax->SetValue(csBuf);
				}

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderMaxLength)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				int nTmp = _tstoi(m_pHeaderMaxLength->GetValue());
				if(nTmp > 50)
				{
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(105));
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONSTOP);

					csBuf.Format(_T("%d"), var.nMaxLength);
					m_pHeaderMaxLength->SetValue(csBuf);
				}
				else if(nTmp == 0)
				{
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(31), var.csName, g_pXML->str(107));
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONSTOP);

					csBuf.Format(_T("%d"), var.nMaxLength);
					m_pHeaderMaxLength->SetValue(csBuf);
				}
				else
				{
					var.nMaxLength = nTmp;
					pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
				}
			}
			else if(pItem == m_pHeaderFactor)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.fFactor = _tstof(CLocale::FixLocale(m_pHeaderFactor->GetValue()));
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderList)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);

				csBuf = var.csList;
				nAmount = csBuf.Replace(_T(";"), _T(";"));

				if(nAmount > NUM_OF_LIST)
				{
					csBuf.Format(g_pXML->str(67), var.csName, NUM_OF_LIST);
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONEXCLAMATION); // "Variable %s has a list with more than %d items. It will be truncated.", "Warning"
				}
			}
			else if(pItem == m_pHeaderScale)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.bScale = m_pHeaderScale->GetBool();
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderBluetooth)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.bBluetooth = m_pHeaderBluetooth->GetBool();
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderDigiTapeDia)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				if(m_pHeaderDigiTapeDia->GetValue() == g_pXML->str(87))	// diameter
					var.nDigiTapeMode = 0;
				else if(m_pHeaderDigiTapeDia->GetValue() == g_pXML->str(88))	// length
					var.nDigiTapeMode = 1;
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem->GetParentItem() == m_pHeaderList)
			{
				// which index has been changed?
				int nIndex2 = 0;
				nIndex2 = pItem->GetIndex() - pItem->GetParentItem()->GetIndex() - 1;

				// add/change it in the list
				if(pItem->GetValue() != _T(""))
					m_caVars.SetAtGrow(nIndex2, pItem->GetValue());
				else
					m_caVars.RemoveAt(nIndex2);

				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.csList = _T("");
				for(int nLoop=0; nLoop<m_caVars.GetSize(); nLoop++)
				{
					csBuf = m_caVars.GetAt(nLoop);
					if(csBuf != _T(""))
					{
						csBuf.Format(_T("%s;"), csBuf);
						var.csList += csBuf;
					}
				}

				pItem->GetParentItem()->SetValue(var.csList);

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderGatorEyes)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.bGatorEyes = m_pHeaderGatorEyes->GetBool();
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderDateformat)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pHeaderDateformat->GetConstraints();
				int nRet = pList->GetCurrent();
				var.nDateformat = nRet;

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderCalculation)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pHeaderCalculation->GetConstraints();
				int nRet = pList->GetCurrent();
				var.nFormula = nRet;

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderParameterList)
			{
			}
			else if(pItem == m_pHeaderPostex)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);

				if(m_pHeaderPostex->GetValue() == g_pXML->str(123))	// x&y
					var.nPostex = 0;
				else if(m_pHeaderPostex->GetValue() == g_pXML->str(124))	// deg&dist 360
					var.nPostex = 1;
				else if(m_pHeaderPostex->GetValue() == g_pXML->str(125))	// deg&dist 400
					var.nPostex = 2;
				else if(m_pHeaderPostex->GetValue() == g_pXML->str(126))	// d1,d2,d3
					var.nPostex = 3;

				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pHeaderPostexDia)
			{
				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.bPostexDia = m_pHeaderPostexDia->GetBool();
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem->GetParentItem() == m_pHeaderParameterList)
			{
				// which index has been changed?
				int nIndex2 = 0;
				nIndex2 = pItem->GetIndex() - pItem->GetParentItem()->GetIndex() - 1;

				// add/change it in the list
				if(pItem->GetValue() != _T(""))
					m_caPars.SetAtGrow(nIndex2, pItem->GetValue());
				else
					m_caPars.RemoveAt(nIndex2);

				var = pDoc->m_caHeaderVars.GetAt(m_nCurIndex);
				var.csParameters = _T("");
				for(int nLoop=0; nLoop<m_caPars.GetSize(); nLoop++)
				{
					csBuf = m_caPars.GetAt(nLoop);
					if(csBuf != _T(""))
					{
						csBuf.Format(_T("%s;"), csBuf);
						var.csParameters += csBuf;
					}
				}

				pItem->GetParentItem()->SetValue(var.csParameters);
				pDoc->m_caHeaderVars.SetAt(m_nCurIndex, var);

				// update the variable lists for the parameters
				UpdateParameters(var.csName);
			}

// ------------------------------------------
// DATA
			else if(pItem == m_pDataName)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				csBuf = m_pDataName->GetValue();
				csBuf.TrimRight('_');
				if(csBuf.GetLength() > 30) csBuf = csBuf.Left(30);

				// change this name in every parameter-list aswell
				for(int nLoop=0; nLoop<pDoc->m_caDataVars.GetCount(); nLoop++)
				{
					varTmp = pDoc->m_caDataVars.GetAt(nLoop);
					if(varTmp.nType == 11)	// calculation
					{
						varTmp.csParameters.Replace(var.csName, csBuf);
						pDoc->m_caDataVars.SetAt(nLoop, varTmp);
					}
				}
				var.csName = csBuf;

				// change variable-name in the list aswell
				m_TreeCtrl.SetItemText(m_itemCurr.hItem, var.csName);

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);

				// update the variable lists for the parameters
				UpdateParameters(var.csName);
			}
			else if(pItem == m_pDataCaption)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.csCaption = m_pDataCaption->GetValue();
				var.csCaption.TrimRight('_');
				if(var.csCaption.GetLength() > 20) var.csCaption = var.csCaption.Left(20);
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataType)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pDataType->GetConstraints();
				int nRet = pList->GetConstraintAt(pList->GetCurrent())->m_dwData;

				// disable all items
				ShowData(FALSE);

				if(nRet == 0)	// int
				{
					m_pDataMin->SetHidden(FALSE);
					m_pDataMax->SetHidden(FALSE);
					m_pDataStep->SetHidden(FALSE);
					m_pDataScale->SetHidden(FALSE);
					m_pDataCounter->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 1)	// float
				{
					m_pDataMin->SetHidden(FALSE);
					m_pDataMax->SetHidden(FALSE);
					m_pDataDecimals->SetHidden(FALSE);
					m_pDataScale->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 2)	// text
				{
					m_pDataMaxLength->SetHidden(FALSE);
					m_pDataScale->SetHidden(FALSE);
					m_pDataBluetooth->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 3)	// list
				{
					m_pDataList->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 4)	// date
				{
					m_pDataScale->SetHidden(FALSE);
					m_pDataDateformat->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 5)	// scale
				{
					m_pDataFactor->SetHidden(FALSE);
					m_pDataGatorEyes->SetHidden(FALSE);
				}
				else if(nRet == 6)	// vertex
				{
					m_pDataFactor->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
					m_pDataBluetooth->SetHidden(FALSE);
				}
				else if(nRet == 8)	// postex
				{
					m_pDataRepeat->SetHidden(FALSE);
					m_pDataBluetooth->SetHidden(FALSE);
					m_pDataPostex->SetHidden(FALSE);
				}
				else if(nRet == 9)	// digitape
				{
					m_pDataDigiTapeDia->SetHidden(FALSE);
					m_pDataFactor->SetHidden(FALSE);
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 10)	// bool
				{
					m_pDataRepeat->SetHidden(FALSE);
				}
				else if(nRet == 11)	// calculation
				{
					// update the variable lists for the parameters
					UpdateParameters(var.csName);

					m_pDataCalculation->SetHidden(FALSE);
					m_pDataParameterList->SetHidden(FALSE);
				}
				else if(nRet == 12)	// DP DME
				{
				}
				else if(nRet == 13)	// DP DME Postex
				{
					m_pDataPostex->SetHidden(FALSE);
					m_pDataPostexDia->SetHidden(FALSE);
				}

				var.nType = nRet;
				SetDataValues(&var);
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataMin)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.nMin = _tstoi(m_pDataMin->GetValue());
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataMax)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.fMax = _tstof(CLocale::FixLocale(m_pDataMax->GetValue()));

				// count how many decimals fMax has and set that in nDecimals.
				var.nDecimals = CountFraction(var.fMax);
				csBuf.Format(_T("%d"), var.nDecimals);
				m_pDataDecimals->SetValue(csBuf);

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataStep)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.nStep = _tstoi(m_pDataStep->GetValue());
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataDecimals)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.nDecimals = _tstoi(m_pDataDecimals->GetValue());

				// add/remove decimals on fMax if needed
				if( CountFraction(var.fMax) != var.nDecimals )
				{
					csBuf.Format(_T("%.*f"), var.nDecimals, var.fMax);
					var.fMax = _tstof(CLocale::FixLocale(csBuf));
					m_pDataMax->SetValue(csBuf);
				}

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataMaxLength)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				
				int nTmp = _tstoi(m_pDataMaxLength->GetValue());
				if(nTmp > 50)
				{
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(105));
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONSTOP);

					csBuf.Format(_T("%d"), var.nMaxLength);
					m_pDataMaxLength->SetValue(csBuf);
				}
				else if(nTmp == 0)
				{
					csBuf.Format(_T("%s \"%s\" %s\r\n"), g_pXML->str(32), var.csName, g_pXML->str(107));
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONSTOP);

					csBuf.Format(_T("%d"), var.nMaxLength);
					m_pDataMaxLength->SetValue(csBuf);
				}
				else
				{
					var.nMaxLength = nTmp;
					pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
				}
			}
			else if(pItem == m_pDataFactor)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.fFactor = _tstof(CLocale::FixLocale(m_pDataFactor->GetValue()));
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataList)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				csBuf = var.csList;
				nAmount = csBuf.Replace(_T(";"), _T(";"));
				if(nAmount > NUM_OF_LIST)
				{
					csBuf.Format(g_pXML->str(67), var.csName, NUM_OF_LIST);
					::MessageBox(this->m_hWnd, csBuf, g_pXML->str(800), MB_OK|MB_ICONEXCLAMATION); // "Variable %s has a list with more than %d items. It will be truncated.", "Warning"
				}
			}
			else if(pItem == m_pDataScale)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bScale = m_pDataScale->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataCounter)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bCounter = m_pDataCounter->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataBluetooth)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bBluetooth = m_pDataBluetooth->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem->GetParentItem() == m_pDataList)
			{
				// which index has been changed?
				int nIndex2 = 0;
				nIndex2 = pItem->GetIndex() - pItem->GetParentItem()->GetIndex() - 1;

				// add/change it in the list
				if(pItem->GetValue() != _T(""))
					m_caVars.SetAtGrow(nIndex2, pItem->GetValue());
				else
					m_caVars.RemoveAt(nIndex2);

				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.csList = _T("");
				for(int nLoop=0; nLoop<m_caVars.GetSize(); nLoop++)
				{
					csBuf = m_caVars.GetAt(nLoop);
					if(csBuf != _T(""))
					{
						csBuf.Format(_T("%s;"), csBuf);
						var.csList += csBuf;
					}
				}

				pItem->GetParentItem()->SetValue(var.csList);
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataDigiTapeDia)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				if(m_pDataDigiTapeDia->GetValue() == g_pXML->str(87))	// diameter
					var.nDigiTapeMode = 0;
				else if(m_pDataDigiTapeDia->GetValue() == g_pXML->str(88))	// length
					var.nDigiTapeMode = 1;
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataGatorEyes)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bGatorEyes = m_pDataGatorEyes->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataDateformat)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pDataDateformat->GetConstraints();
				int nRet = pList->GetCurrent();
				var.nDateformat = nRet;

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataCalculation)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				CXTPPropertyGridItemConstraints* pList = m_pDataCalculation->GetConstraints();
				int nRet = pList->GetCurrent();
				var.nFormula = nRet;

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataParameterList)
			{
			}
			else if(pItem == m_pDataRepeat)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bRepeat = m_pDataRepeat->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataPostex)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);

				if(m_pDataPostex->GetValue() == g_pXML->str(123))	// x&y
					var.nPostex = 0;
				else if(m_pDataPostex->GetValue() == g_pXML->str(124))	// deg&dist 360
					var.nPostex = 1;
				else if(m_pDataPostex->GetValue() == g_pXML->str(125))	// deg&dist 400
					var.nPostex = 2;
				else if(m_pDataPostex->GetValue() == g_pXML->str(126))	// d1,d2,d3
					var.nPostex = 3;

				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem == m_pDataPostexDia)
			{
				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.bPostexDia = m_pDataPostexDia->GetBool();
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);
			}
			else if(pItem->GetParentItem() == m_pDataParameterList)
			{
				// which index has been changed?
				int nIndex2 = 0;
				nIndex2 = pItem->GetIndex() - pItem->GetParentItem()->GetIndex() - 1;

				// add/change it in the list
				if(pItem->GetValue() != _T(""))
					m_caPars.SetAtGrow(nIndex2, pItem->GetValue());
				else
					m_caPars.RemoveAt(nIndex2);

				var = pDoc->m_caDataVars.GetAt(m_nCurIndex);
				var.csParameters = _T("");
				for(int nLoop=0; nLoop<m_caPars.GetSize(); nLoop++)
				{
					csBuf = m_caPars.GetAt(nLoop);
					if(csBuf != _T(""))
					{
						csBuf.Format(_T("%s;"), csBuf);
						var.csParameters += csBuf;
					}
				}

				pItem->GetParentItem()->SetValue(var.csParameters);
				pDoc->m_caDataVars.SetAt(m_nCurIndex, var);

				// update the variable lists for the parameters
				UpdateParameters(var.csName);
			}


// ------------------------------------------
// TEXT
			else if(pItem == m_pTextCaption)
			{
				text = pDoc->m_caTexts.GetAt(m_nCurIndex);
				text.csCaption = m_pTextCaption->GetValue();

				// change variable-name in the list aswell
				m_TreeCtrl.SetItemText(m_itemCurr.hItem, text.csCaption);

				pDoc->m_caTexts.SetAt(m_nCurIndex, text);
			}
		}
		break;
	}

	return FALSE;
}

/*---------------------------------------------------------------
	Update the view
---------------------------------------------------------------*/
void CProgramsDlg::OnUpdate(CView *pSender, LPARAM lHint, CObject *pHint)
{
	int nLoop;
	VAR_TYPE var;
	TEXT_TYPE text;

	if(pDoc == NULL)
		return;

	// only enable things if we have a file
	m_wndPropertyGrid.EnableWindow(m_bGotDoc);
	m_TreeCtrl.EnableWindow(m_bGotDoc);
	((CProgramsFrame*)GetParent())->SetButtonEnabled(4, m_bGotDoc);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, m_bGotDoc);

	m_TreeCtrl.DeleteAllItems();

	HTREEITEM htiTemp;
	HTREEITEM htiHeader = m_TreeCtrl.InsertItem(g_pXML->str(1), TYPE_HEADER, TYPE_HEADER); //0, 1);	// header variables
	m_TreeCtrl.SetItemBold(htiHeader);
	m_TreeCtrl.FocusItem(htiHeader);

	for(nLoop=0; nLoop<pDoc->m_caHeaderVars.GetSize(); nLoop++)
	{
		var = pDoc->m_caHeaderVars.GetAt(nLoop);
		htiTemp = m_TreeCtrl.InsertItem(var.csName, TYPE_HEADER, TYPE_HEADER, htiHeader);
	}


	HTREEITEM htiData = m_TreeCtrl.InsertItem(g_pXML->str(2), TYPE_DATA, TYPE_DATA); //0, 1);	// data variables
	m_TreeCtrl.SetItemBold(htiData);

	for(nLoop=0; nLoop<pDoc->m_caDataVars.GetSize(); nLoop++)
	{
		var = pDoc->m_caDataVars.GetAt(nLoop);
		htiTemp = m_TreeCtrl.InsertItem(var.csName, TYPE_DATA, TYPE_DATA, htiData);
	}



	HTREEITEM htiText = m_TreeCtrl.InsertItem(g_pXML->str(3), 0, 1);	// program text
	m_TreeCtrl.SetItemBold(htiText);

	int nIndex = 0;
	htiTemp = m_TreeCtrl.InsertItem(g_pXML->str(4), 0, 1, htiText);	// main menu
	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);


	htiTemp = m_TreeCtrl.InsertItem(g_pXML->str(5), 0, 1, htiText);	// measure menu
	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);


	htiTemp = m_TreeCtrl.InsertItem(g_pXML->str(6), 0, 1, htiText);	// settings menu
	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);


	htiTemp = m_TreeCtrl.InsertItem(g_pXML->str(7), 0, 1, htiText);	// various text
	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);	// yes
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);	// no
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);	// value to big!
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);

	text = pDoc->m_caTexts.GetAt(nIndex++);	// value to small!
	m_TreeCtrl.InsertItem(text.csCaption, TYPE_TEXT, TYPE_TEXT, htiTemp);


	m_TreeCtrl.Expand(htiHeader, TVE_EXPAND);
	m_TreeCtrl.Expand(htiData, TVE_EXPAND);
	m_TreeCtrl.Expand(htiText, TVE_EXPAND);

	m_TreeCtrl.SetFont(&XTAuxData().font);
}

/*---------------------------------------------------------------
	Add a variable
---------------------------------------------------------------*/
void CProgramsDlg::OnBnClickedAdd()
{
	// what have we selected, a parent-item or a child-item?
	HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
	TVITEM item;
	TCHAR szText[1024];
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	item.pszText = szText;
	item.cchTextMax = 1024;
	m_TreeCtrl.GetItem(&item);

	VAR_TYPE var;
	var.csList = _T("");
	var.nDecimals = 0;
	var.fFactor = 0.0;
	var.fMax = 0.0;
	var.nStep = 0;
	var.nMaxLength = 5;
	var.nMin = 0;
	var.nType = 2;	// text
	var.bScale = FALSE;
	var.bCounter = FALSE;
	var.nPostex = 1;	// deg&dist 360
	var.bBluetooth = FALSE;
	var.nDigiTapeMode = 1;	// 0 = diameter, 1 = length
	var.bGatorEyes = FALSE;
	var.nDateformat = 0;	// YYYY/MM/DD
	var.nFormula = 0;
	var.csParameters = _T("");
	var.bRepeat = FALSE;
	var.bPostexDia = FALSE;

	HTREEITEM hParent = NULL;
	if(item.iImage == TYPE_HEADER)
	{
		if(_tcscmp(szText, g_pXML->str(1)) == 0)	// "header variables"
		{
			hParent = hItem;
			hItem = TVI_LAST;
		}
		else
		{
			hParent = m_TreeCtrl.GetParentItem(hItem);
		}

		if(pDoc->m_caHeaderVars.GetSize() >= NUM_OF_HEADERS) return;
		var.csCaption.Format(_T("%s%d"), g_pXML->str(93), pDoc->m_caHeaderVars.GetSize()+1);	// new var
		var.csName.Format(_T("%s %d"), g_pXML->str(33), pDoc->m_caHeaderVars.GetSize()+1);

		if(hItem == TVI_LAST)	// add variable last
		{
			pDoc->m_caHeaderVars.Add(var);
		}
		else
		{
			pDoc->m_caHeaderVars.InsertAt(m_nSelectedIndex+1, var);
		}
	}
	else if(item.iImage == TYPE_DATA)
	{
		if(_tcscmp(szText, g_pXML->str(2)) == 0)	// "data variables"
		{
			hParent = hItem;
			hItem = TVI_LAST;
		}
		else
		{
			hParent = m_TreeCtrl.GetParentItem(hItem);
		}

		if(pDoc->m_caDataVars.GetSize() >= NUM_OF_DATAS) return;
		var.csCaption.Format(_T("%s%d"), g_pXML->str(93), pDoc->m_caDataVars.GetSize()+1);	// new var
		var.csName.Format(_T("%s %d"), g_pXML->str(33), pDoc->m_caDataVars.GetSize()+1);

		if(hItem == TVI_LAST)	// add variable last
		{
			pDoc->m_caDataVars.Add(var);
		}
		else
		{
			pDoc->m_caDataVars.InsertAt(m_nSelectedIndex+1, var);
		}
	}
		
	_tcscpy(szText, var.csName);	// new var
	hItem = m_TreeCtrl.InsertItem(item.pszText, item.iImage, item.iSelectedImage, hParent, hItem);
	m_TreeCtrl.SelectItem(hItem);
	m_TreeCtrl.Invalidate();

	pDoc->SetModifiedFlag(TRUE);

	// update the variable lists for the parameters
	UpdateParameters();
}

/*---------------------------------------------------------------
	Delete a variable
---------------------------------------------------------------*/
void CProgramsDlg::OnBnClickedDel()
{
	HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
	TVITEM item;
	TCHAR szText[1024];
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	item.pszText = szText;
	item.cchTextMax = 1024;
	m_TreeCtrl.GetItem(&item);

	if(item.iImage == TYPE_HEADER ||
		item.iImage == TYPE_DATA)
	{
		int nRet;
		nRet = MessageBox(g_pXML->str(106), g_pXML->str(800), MB_YESNO|MB_ICONEXCLAMATION);
		if(nRet == IDNO) return;
	}
	else
	{
		return;
	}

	// move the data in the array
	VAR_TYPE var;
	if(item.iImage == TYPE_HEADER)
	{
		pDoc->m_caHeaderVars.RemoveAt(m_nSelectedIndex);
	}
	else if(item.iImage == TYPE_DATA)
	{
		pDoc->m_caDataVars.RemoveAt(m_nSelectedIndex);
	}

	// move the item in the tree
	m_TreeCtrl.DeleteItem(hItem);

	pDoc->SetModifiedFlag(TRUE);

	// update the variable lists for the parameters
	UpdateParameters();
}

/*---------------------------------------------------------------
	Move variable UP
---------------------------------------------------------------*/
void CProgramsDlg::OnBnClickedUp()
{
	HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
	TVITEM item;
	TCHAR szText[1024];
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	item.pszText = szText;
	item.cchTextMax = 1024;
	m_TreeCtrl.GetItem(&item);

	// move the variable in the data-array
	VAR_TYPE var;
	if(item.iImage == TYPE_HEADER)
	{
		var = pDoc->m_caHeaderVars.GetAt(m_nSelectedIndex);
		pDoc->m_caHeaderVars.RemoveAt(m_nSelectedIndex);
		if(m_nSelectedIndex != 0)
			pDoc->m_caHeaderVars.InsertAt(m_nSelectedIndex-1, var);
		else
			pDoc->m_caHeaderVars.InsertAt(m_nSelectedIndex, var);
	}
	else if(item.iImage == TYPE_DATA)
	{
		var = pDoc->m_caDataVars.GetAt(m_nSelectedIndex);
		pDoc->m_caDataVars.RemoveAt(m_nSelectedIndex);
		if(m_nSelectedIndex != 0)
			pDoc->m_caDataVars.InsertAt(m_nSelectedIndex-1, var);
		else
			pDoc->m_caDataVars.InsertAt(m_nSelectedIndex, var);
	}


	// move in the tree aswell
	HTREEITEM hParent = m_TreeCtrl.GetParentItem(hItem);
	HTREEITEM hPrev = m_TreeCtrl.GetPrevSiblingItem(hItem);
	hPrev = m_TreeCtrl.GetPrevSiblingItem(hPrev);

	m_TreeCtrl.DeleteItem(hItem);
	if(hPrev != NULL)
		hItem = m_TreeCtrl.InsertItem(item.pszText, item.iImage, item.iSelectedImage, hParent, hPrev);
	else
		hItem = m_TreeCtrl.InsertItem(item.pszText, item.iImage, item.iSelectedImage, hParent, TVI_FIRST);

	m_TreeCtrl.SelectItem(hItem);

	pDoc->SetModifiedFlag(TRUE);

	// update the variable lists for the parameters
	UpdateParameters();
}

/*---------------------------------------------------------------
	Move variable DOWN
---------------------------------------------------------------*/
void CProgramsDlg::OnBnClickedDown()
{
	HTREEITEM hItem = m_TreeCtrl.GetFirstSelectedItem();
	TVITEM item;
	TCHAR szText[1024];
	item.hItem = hItem;
	item.mask = TVIF_TEXT | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	item.pszText = szText;
	item.cchTextMax = 1024;
	m_TreeCtrl.GetItem(&item);

	// move the data in the array
	VAR_TYPE var;
	if(item.iImage == TYPE_HEADER)
	{
		var = pDoc->m_caHeaderVars.GetAt(m_nSelectedIndex);
		pDoc->m_caHeaderVars.RemoveAt(m_nSelectedIndex);
		pDoc->m_caHeaderVars.InsertAt(m_nSelectedIndex+1, var);
	}
	else if(item.iImage == TYPE_DATA)
	{
		var = pDoc->m_caDataVars.GetAt(m_nSelectedIndex);
		pDoc->m_caDataVars.RemoveAt(m_nSelectedIndex);
		pDoc->m_caDataVars.InsertAt(m_nSelectedIndex+1, var);
	}

	// move the item in the tree
	HTREEITEM hNext = m_TreeCtrl.GetNextSiblingItem(hItem);
	m_TreeCtrl.DeleteItem(hItem);
	hItem = m_TreeCtrl.InsertItem(item.pszText, item.iImage, item.iSelectedImage, m_TreeCtrl.GetParentItem(hNext), hNext);

	m_TreeCtrl.SelectItem(hItem);

	pDoc->SetModifiedFlag(TRUE);

	// update the variable lists for the parameters
	UpdateParameters();
}

/*---------------------------------------------------------------
	Send XML to caliper
---------------------------------------------------------------*/
void CProgramsDlg::OnBnClickedSend()
{
	BOOL sendVhxFile=TRUE;

	if(pDoc == NULL || pDoc->GetPathName().IsEmpty() || pDoc->IsModified())
	{ 
		//Show a message if anything has been modified, and not saved.
		sendVhxFile=MessageBox(g_pXML->str(117), g_pXML->str(800), MB_YESNO|MB_ICONEXCLAMATION) == IDYES;

		if(sendVhxFile)
		{ //If the user agree to save&send, then first show the save vhx file dialog.
			sendVhxFile=pDoc->VbxFileDialog();
		}
	}

	if(sendVhxFile)
	{
		// tell the Communication-suite to send the file
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), _T(""), pDoc->GetPathName(), _T("")));
		
		//CString csBuf = _T("C:\\Users\\anders.HAGLOF\\Desktop\\1.txt%C:\\Users\\anders.HAGLOF\\Desktop\\2.txt%C:\\Users\\anders.HAGLOF\\Desktop\\3.txt");
		//AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), _T(""), pDoc->GetPathName(), _T(""), (LPTSTR)csBuf.GetBuffer()));
	}
}


/*---------------------------------------------------------------
	New document
---------------------------------------------------------------*/
void CProgramsDlg::OnNew()
{
	pDoc->OnNewDocument();

	m_bGotDoc = TRUE;

	pDoc->UpdateAllViews(NULL);
}

/*---------------------------------------------------------------
	Open old document
---------------------------------------------------------------*/
void CProgramsDlg::OnOpen()
{
	m_bGotDoc=pDoc->VbxFileDialog(TRUE);
	pDoc->UpdateAllViews(NULL);
}

/*---------------------------------------------------------------
	Save current document
---------------------------------------------------------------*/
void CProgramsDlg::OnSave()
{
	if(!theApp.CheckLicense())
	{
		::MessageBox(this->m_hWnd, g_pXML->str(75), g_pXML->str(800), MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	pDoc->VbxFileDialog();
}

void CProgramsDlg::UpdateParameters(CString csName)
{
	// update the calculating variables for the header
	VAR_TYPE varTmp;
	CXTPPropertyGridItemConstraints* pList;
	CXTPPropertyGridItems *pItems = m_pHeaderParameterList->GetChilds();
	for(int nLoop=0; nLoop<pItems->GetCount(); nLoop++)
	{
		pList = pItems->GetAt(nLoop)->GetConstraints();
		pList->RemoveAll();

		for(int nLoop2=0; nLoop2<pDoc->m_caHeaderVars.GetCount(); nLoop2++)
		{
			varTmp = pDoc->m_caHeaderVars.GetAt(nLoop2);

			if(csName == _T(""))
				pList->AddConstraint(varTmp.csName);
			else if(varTmp.csName != csName)
				pList->AddConstraint(varTmp.csName);
		}
		pList->AddConstraint(_T(""));
	}

	// update the calculating variables for the data
	pItems = m_pDataParameterList->GetChilds();
	for(int nLoop=0; nLoop<pItems->GetCount(); nLoop++)
	{
		pList = pItems->GetAt(nLoop)->GetConstraints();
		pList->RemoveAll();

		for(int nLoop2=0; nLoop2<pDoc->m_caDataVars.GetCount(); nLoop2++)
		{
			varTmp = pDoc->m_caDataVars.GetAt(nLoop2);

			if(csName == _T(""))
				pList->AddConstraint(varTmp.csName);
			else if(varTmp.csName != csName)
				pList->AddConstraint(varTmp.csName);
		}
		pList->AddConstraint(_T(""));
	}
}

void CProgramsDlg::SetHeaderValues(VAR_TYPE *pVar)
{
	CString csBuf;
	CXTPPropertyGridItemConstraints* pList = m_pHeaderType->GetConstraints();

	csBuf.Format(_T("%d"), pVar->nMin); m_pHeaderMin->SetValue(csBuf);
	if(pVar->nType == 0)	// int
	{
		csBuf.Format(_T("%g"), pVar->fMax); m_pHeaderMax->SetValue(csBuf);
	}
	else if(pVar->nType == 1)
	{
		csBuf.Format(_T("%.*f"), pVar->nDecimals, pVar->fMax); m_pHeaderMax->SetValue(csBuf);
	}
	csBuf.Format(_T("%d"), pVar->nStep); m_pHeaderStep->SetValue(csBuf);
	csBuf.Format(_T("%d"), pVar->nDecimals); m_pHeaderDecimals->SetValue(csBuf);
	csBuf.Format(_T("%d"), pVar->nMaxLength); m_pHeaderMaxLength->SetValue(csBuf);
	csBuf.Format(_T("%f"), pVar->fFactor); m_pHeaderFactor->SetValue(csBuf);
	if(pVar->nDigiTapeMode == 0)	// diameter
		m_pHeaderDigiTapeDia->SetValue(g_pXML->str(87));
	else if(pVar->nDigiTapeMode == 1)	// length
		m_pHeaderDigiTapeDia->SetValue(g_pXML->str(88));
	m_pHeaderList->SetValue(pVar->csList);
	m_pHeaderScale->SetBool(pVar->bScale);
	m_pHeaderBluetooth->SetBool(pVar->bBluetooth);
	m_pHeaderGatorEyes->SetBool(pVar->bGatorEyes);
	pList = m_pHeaderDateformat->GetConstraints();
	m_pHeaderDateformat->SetValue(pList->GetAt(pVar->nDateformat));
	pList = m_pHeaderCalculation->GetConstraints();
	m_pHeaderCalculation->SetValue(pList->GetAt(pVar->nFormula));
	m_pHeaderParameterList->SetValue(pVar->csParameters);
	m_pHeaderPostexDia->SetBool(pVar->bPostexDia);
	
	int nCurPos = 0;
	m_caVars.RemoveAll();
	CString resToken = pVar->csList.Tokenize(_T(";"), nCurPos);
	while (nCurPos != -1)
	{
		m_caVars.Add(resToken);
		resToken = pVar->csList.Tokenize(_T(";"), nCurPos);
	};

	nCurPos = 0;
	m_caPars.RemoveAll();
	resToken = pVar->csParameters.Tokenize(_T(";"), nCurPos);
	while (nCurPos != -1)
	{
		m_caPars.Add(resToken);
		resToken = pVar->csParameters.Tokenize(_T(";"), nCurPos);
	};

	if(pVar->nPostex == 0)	// x&y
		m_pHeaderPostex->SetValue(g_pXML->str(123));
	else if(pVar->nPostex == 1)	// deg&dist 360
		m_pHeaderPostex->SetValue(g_pXML->str(124));
	else if(pVar->nPostex == 2)	// deg&dist 400
		m_pHeaderPostex->SetValue(g_pXML->str(125));
	else if(pVar->nPostex == 3)	// d1,d2,d3
		m_pHeaderPostex->SetValue(g_pXML->str(126));
}

void CProgramsDlg::SetDataValues(VAR_TYPE *pVar)
{
	CString csBuf;
	CXTPPropertyGridItemConstraints* pList = m_pDataType->GetConstraints();

	csBuf.Format(_T("%d"), pVar->nMin); m_pDataMin->SetValue(csBuf);
	if(pVar->nType == 0)	// int
	{
		csBuf.Format(_T("%g"), pVar->fMax); m_pDataMax->SetValue(csBuf);
	}
	else if(pVar->nType == 1)
	{
		csBuf.Format(_T("%.*f"), pVar->nDecimals, pVar->fMax); m_pDataMax->SetValue(csBuf);
	}
	csBuf.Format(_T("%d"), pVar->nStep); m_pDataStep->SetValue(csBuf);
	csBuf.Format(_T("%d"), pVar->nDecimals); m_pDataDecimals->SetValue(csBuf);
	csBuf.Format(_T("%d"), pVar->nMaxLength); m_pDataMaxLength->SetValue(csBuf);
	csBuf.Format(_T("%f"), pVar->fFactor); m_pDataFactor->SetValue(csBuf);
	if(pVar->nDigiTapeMode == 0)	// diameter
		m_pDataDigiTapeDia->SetValue(g_pXML->str(87));
	else if(pVar->nDigiTapeMode == 1)	// length
		m_pDataDigiTapeDia->SetValue(g_pXML->str(88));
	m_pDataList->SetValue(pVar->csList);
	m_pDataScale->SetBool(pVar->bScale);
	m_pDataCounter->SetBool(pVar->bCounter);
	m_pDataBluetooth->SetBool(pVar->bBluetooth);
	m_pDataGatorEyes->SetBool(pVar->bGatorEyes);
	pList = m_pDataDateformat->GetConstraints();
	m_pDataDateformat->SetValue(pList->GetAt(pVar->nDateformat));
	pList = m_pDataCalculation->GetConstraints();
	m_pDataCalculation->SetValue(pList->GetAt(pVar->nFormula));
	m_pDataParameterList->SetValue(pVar->csParameters);
	m_pDataRepeat->SetBool(pVar->bRepeat);
	m_pDataPostexDia->SetBool(pVar->bPostexDia);

	if(pVar->nPostex == 0)	// x&y
		m_pDataPostex->SetValue(g_pXML->str(123));
	else if(pVar->nPostex == 1)	// deg&dist 360
		m_pDataPostex->SetValue(g_pXML->str(124));
	else if(pVar->nPostex == 2)	// deg&dist 400
		m_pDataPostex->SetValue(g_pXML->str(125));
	else if(pVar->nPostex == 3)	// d1,d2,d3
		m_pDataPostex->SetValue(g_pXML->str(126));
}

void CProgramsDlg::ShowHeaders(BOOL bShow)
{
	m_pHeaderMin->SetHidden(!bShow);
	m_pHeaderMax->SetHidden(!bShow);
	m_pHeaderStep->SetHidden(!bShow);
	m_pHeaderDecimals->SetHidden(!bShow);
	m_pHeaderMaxLength->SetHidden(!bShow);
	m_pHeaderFactor->SetHidden(!bShow);
	m_pHeaderList->SetHidden(!bShow);
	m_pHeaderScale->SetHidden(!bShow);
	m_pHeaderBluetooth->SetHidden(!bShow);
	m_pHeaderDigiTapeDia->SetHidden(!bShow);
	m_pHeaderGatorEyes->SetHidden(!bShow);
	m_pHeaderDateformat->SetHidden(!bShow);
	m_pHeaderCalculation->SetHidden(!bShow);
	m_pHeaderParameterList->SetHidden(!bShow);
	m_pHeaderPostex->SetHidden(!bShow);
	m_pHeaderPostexDia->SetHidden(!bShow);
}

void CProgramsDlg::ShowData(BOOL bShow)
{
	m_pDataMin->SetHidden(!bShow);
	m_pDataMax->SetHidden(!bShow);
	m_pDataStep->SetHidden(!bShow);
	m_pDataDecimals->SetHidden(!bShow);
	m_pDataMaxLength->SetHidden(!bShow);
	m_pDataFactor->SetHidden(!bShow);
	m_pDataList->SetHidden(!bShow);
	m_pDataScale->SetHidden(!bShow);
	m_pDataCounter->SetHidden(!bShow);
	m_pDataBluetooth->SetHidden(!bShow);
	m_pDataDigiTapeDia->SetHidden(!bShow);
	m_pDataGatorEyes->SetHidden(!bShow);
	m_pDataDateformat->SetHidden(!bShow);
	m_pDataCalculation->SetHidden(!bShow);
	m_pDataParameterList->SetHidden(!bShow);
	m_pDataRepeat->SetHidden(!bShow);
	m_pDataPostex->SetHidden(!bShow);
	m_pDataPostexDia->SetHidden(!bShow);
}
