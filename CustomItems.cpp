// Custom Items.cpp : implementation file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "CustomItems.h"
#include "ProgramsDlg.h"

////////////////////////////////////////////////////////////////////////////////////////////////

CCustomItemButton::CCustomItemButton(CString strCaption, CString strValue, BOOL bComboButton)
	: CXTPPropertyGridItem(strCaption, strValue)
{
	m_csValue = strValue;
	m_bComboButton = bComboButton;
}

CCustomItemButton::~CCustomItemButton(void)
{
}

void CCustomItemButton::OnInplaceButtonDown()
{
}

void CCustomItemButton::SetValue(CString strValue)
{
	m_csValue = strValue;

	this->GetChilds()->Clear();

	int nPos = 0, nLines=1;
	CString csToken = m_csValue.Tokenize(_T(";"), nPos);
	CString csBuf;
	while(nPos != -1)
	{
		csBuf.Format(_T("%d"), nLines);
		CXTPPropertyGridItem *pItem = this->AddChildItem(new CXTPPropertyGridItem(csBuf, csToken));
		if(m_bComboButton == TRUE) pItem->SetFlags(xtpGridItemHasComboButton);

		csToken = m_csValue.Tokenize(_T(";"), nPos);
		nLines++;
	};
	if(nLines <= NUM_OF_LIST)
	{
		csBuf.Format(_T("%d"), nLines);
		CXTPPropertyGridItem *pItem = this->AddChildItem(new CXTPPropertyGridItem(csBuf, _T("")));
		if(m_bComboButton == TRUE) pItem->SetFlags(xtpGridItemHasComboButton);
	}


	CXTPPropertyGridItem::SetValue(m_csValue);
}
