#pragma once

#ifndef _ProgramsDlg_H_
#define _ProgramsDlg_H_


#include "Resource.h"
#include "afxcmn.h"
#include "xmlParser.h"
#include "afxwin.h"
#include "SampleSuiteForms.h"
#include "MyLocale.h"

// limits for the amount of variables and such
#define NUM_OF_HEADERS 20
#define NUM_OF_DATAS 50
#define NUM_OF_LIST 50

//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropGridItemBool : public CXTPPropertyGridItemBool
{
public:
	CMyPropGridItemBool(const CString true_text,
						const CString false_text,
						const CString& cap,
						BOOL bValue) : CXTPPropertyGridItemBool(cap, bValue)
	{
		SetTrueFalseText((true_text), (false_text));
	}
};

typedef struct _tagTEXT
{
	CString csName;
	CString csCaption;
} TEXT_TYPE;

typedef struct _tagPARAM
{
	CString csName;
	int nIndex;
} PARAMETER_TYPE;

typedef struct _tagVARIABLE
{
	CString csName;		// name of element
	CString csCaption;	// text to show

	int nType;			// variable type (0 = int, 1 = float, 2 = text, 3 = list, 4 = date, 5 = scale, 6 = vertex, 7 = gps, 8 = postex, 9 = DigiTape, 10 = bool, 11 = calculation, 12 = DPDME, 13 = DPDMEPOSTEX, 14 = DPDMEGPS)

	int nMin;		// (if int)
	float fMax;		// (if int)
	int nStep;		// (if int)
	int nDecimals;	// (if float)
	int nMaxLength;	// (if text)
	float fFactor;	// (scale-divisor if diameter or vertex)
	CString csList;	// (semicolon-separated items if type is list)
	BOOL bScale;	// use scale to enter numbers/text?
	BOOL bCounter;	// should this be an increasing variable?
	int nPostex;	// 0 = X&Y, 1 = deg&dist (360), 2 = deg&dist (400), 3 = d1,d2,d3
	BOOL bBluetooth;	// use external Bluetooth-device to gather data
	int nDigiTapeMode;	// 0 = diameter, 1 = length
	BOOL bGatorEyes;	// add a bit more to the diameters?
	int nDateformat;	// 0 = YYYY/MM/DD, 1 = MM/DD/YYYY
	int nFormula;		// which formula to use. (if variable type is calculation)
	CString csParameters;	// which parameters to use. (if variable type is calculation)
	BOOL bRepeat;		// repeat previous entered value for the variable
	BOOL bPostexDia;	// used for DPDMEPOSTEX to add diameter or not
} VAR_TYPE;

/*------------------------------------------------------------
  Document class
------------------------------------------------------------*/
class CProgramsDoc : public CDocument
{
protected: // create from serialization only
	CProgramsDoc();
	DECLARE_DYNCREATE(CProgramsDoc)

// Attributes
public:
	// DATA
	CArray<VAR_TYPE> m_caHeaderVars;
	CArray<VAR_TYPE> m_caDataVars;
	CArray<TEXT_TYPE> m_caTexts;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnFileSave();
	BOOL VbxFileDialog(BOOL aOpenFile=FALSE);
	//}}AFX_VIRTUAL

	void ConvertToHtml(CString*);

// Implementation
public:
	virtual ~CProgramsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
#define CRC32_POLYNOMIAL 0xEDB88320

unsigned long CRC32Value(int i)
{
	int j;
	unsigned long ulCRC;
	ulCRC = i;
	for (j=8;j>0;j--)
	{
		if (ulCRC & 1)
			ulCRC = (ulCRC >> 1)^CRC32_POLYNOMIAL;
		else
			ulCRC >>= 1;
	}
	return ulCRC;
}

unsigned long CalculateBlockCRC32(unsigned long ulCount, unsigned char *ucBuffer)
{
	unsigned long ulTemp1;
	unsigned long ulTemp2;
	unsigned long ulCRC = 0;
	while (ulCount-- != 0)
	{
		ulTemp1 = (ulCRC >> 8) & 0x00FFFFFFL;
		ulTemp2 = CRC32Value(((int)ulCRC^*ucBuffer++)&0xff);
		ulCRC = ulTemp1^ulTemp2;
	}
	return(ulCRC);
}

// Generated message map functions
protected:
	//{{AFX_MSG(CProgramsDoc)
	//}}AFX_MSG
	virtual BOOL SaveModified( );
	DECLARE_MESSAGE_MAP()
	int CountInt(int nNumber);
};

/*------------------------------------------------------------
  Frame class
------------------------------------------------------------*/
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CProgramsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CProgramsFrame)

	BOOL m_bOnce;

public:
	CProgramsFrame();

	// Toolbar
	CXTPToolBar m_wndToolBar;
	CStatusBar  m_wndStatusBar;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
	//{{AFX_MSG(CProgramsFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
};

/*------------------------------------------------------------
  Dialog (view) class
------------------------------------------------------------*/
class CProgramsDlg : public CXTResizeFormView
{
protected: // create from serialization only
	CProgramsDlg();
	DECLARE_DYNCREATE(CProgramsDlg)

// Attributes
public:
	enum { IDD = IDD_PROGRAMS };
	CProgramsDoc* pDoc;

private:
	CXTTreeCtrl m_TreeCtrl;
	CImageList m_ImageList;
	CStatic m_wndPlaceHolder;
	int m_nSelectedIndex;
	CStringArray m_caVars;
	CStringArray m_caPars;
//	CArray<PARAMETER_TYPE> m_caPars;
	BOOL m_bGotDoc;

// Operations
public:

private:
	CXTPPropertyGrid m_wndPropertyGrid;

	// header
	CXTPPropertyGridItem *m_pHeader;
	CXTPPropertyGridItem *m_pHeaderName;
	CXTPPropertyGridItem *m_pHeaderCaption;
	CXTPPropertyGridItem *m_pHeaderType;
	CXTPPropertyGridItem *m_pHeaderMin;
	CXTPPropertyGridItem *m_pHeaderMax;
	CXTPPropertyGridItem *m_pHeaderStep;
	CXTPPropertyGridItem *m_pHeaderDecimals;
	CXTPPropertyGridItem *m_pHeaderMaxLength;
	CXTPPropertyGridItem *m_pHeaderFactor;
	CXTPPropertyGridItem *m_pHeaderList;
	CXTPPropertyGridItemBool *m_pHeaderScale;
	CXTPPropertyGridItemBool *m_pHeaderBluetooth;
	CXTPPropertyGridItem *m_pHeaderDigiTapeDia;	// output diameter or length when measuring with DigiTape
	CXTPPropertyGridItemBool *m_pHeaderGatorEyes;
	CXTPPropertyGridItem *m_pHeaderDateformat;
	CXTPPropertyGridItem *m_pHeaderCalculation;
	CXTPPropertyGridItem *m_pHeaderParameterList;
	CXTPPropertyGridItem *m_pHeaderPostex;
	CXTPPropertyGridItemBool *m_pHeaderPostexDia;

	// data
	CXTPPropertyGridItem *m_pData;
	CXTPPropertyGridItem *m_pDataName;
	CXTPPropertyGridItem *m_pDataCaption;
	CXTPPropertyGridItem *m_pDataType;
	CXTPPropertyGridItem *m_pDataMin;
	CXTPPropertyGridItem *m_pDataMax;
	CXTPPropertyGridItem *m_pDataStep;
	CXTPPropertyGridItem *m_pDataDecimals;
	CXTPPropertyGridItem *m_pDataMaxLength;
	CXTPPropertyGridItem *m_pDataFactor;
	CXTPPropertyGridItem *m_pDataList;
	CXTPPropertyGridItemBool *m_pDataScale;
	CXTPPropertyGridItemBool *m_pDataCounter;
	CXTPPropertyGridItemBool *m_pDataBluetooth;
	CXTPPropertyGridItem *m_pDataDigiTapeDia;	// output diameter or length when measuring with DigiTape
	CXTPPropertyGridItemBool *m_pDataGatorEyes;
	CXTPPropertyGridItem *m_pDataDateformat;
	CXTPPropertyGridItem *m_pDataCalculation;
	CXTPPropertyGridItem *m_pDataParameterList;
	CXTPPropertyGridItemBool *m_pDataRepeat;
	CXTPPropertyGridItem *m_pDataPostex;
	CXTPPropertyGridItemBool *m_pDataPostexDia;

	// text
	CXTPPropertyGridItem *m_pText;
	CXTPPropertyGridItem *m_pTextName;
	CXTPPropertyGridItem *m_pTextCaption;

	int m_nCurIndex;	// currently selected item
	TVITEM m_itemCurr;	// currently selected item

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsDlg)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
public:

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//{{AFX_MSG(CProgramsDlg)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDestroy();
	afx_msg void OnRclickTreeCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdate(CView *pSender, LPARAM lHint, CObject *pHint);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void UpdateParameters(CString csName = _T(""));
	void SetHeaderValues(VAR_TYPE *pVar);
	void SetDataValues(VAR_TYPE *pVar);
	void ShowHeaders(BOOL bShow);
	void ShowData(BOOL bShow);
public:
	afx_msg void OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult);
	void OnNew();
	void OnOpen();
	void OnSave();
	void OnBnClickedAdd();
	void OnBnClickedDel();
	void OnBnClickedUp();
	void OnBnClickedDown();
	void OnBnClickedSend();
	LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
};
#endif
