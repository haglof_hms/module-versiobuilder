// MessageRecord.h: interface for the CMessageRecord class.
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGERECORD_H__A08F955C_1EA1_40B4_A18F_D2B7857FB244__INCLUDED_)
#define AFX_MESSAGERECORD_H__A08F955C_1EA1_40B4_A18F_D2B7857FB244__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CReportSampleView;

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CMessageRecordItemCheck : public CXTPReportRecordItem
{
	DECLARE_SERIAL(CMessageRecordItemCheck)
public:
	// Constructs record item with the initial checkbox value.
	CMessageRecordItemCheck(BOOL bCheck = FALSE);

	// Provides custom group captions depending on checkbox value.
	// Returns caption string ID to be read from application resources.
	virtual int GetGroupCaptionID(CXTPReportColumn* pColumn);

	// Provides custom records comparison by this item based on checkbox value, 
	// instead of based on captions.
	virtual int Compare(CXTPReportColumn* pColumn, CXTPReportRecordItem* pItem);
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CMessageRecordItemCombo : public CXTPReportRecordItem
{
	DECLARE_SERIAL(CMessageRecordItemCombo)
public:
	// Constructs record item with the initial checkbox value.
	CMessageRecordItemCombo(int nLevel = 0);

	CXTPReportRecordItemEditOptions m_aaa;

	void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS*, CXTPReportRecordItemConstraint* pConstraint);
	CString GetCaption(CXTPReportColumn* /*pColumn*/);

protected:

	int m_nValue;
};


class CMessageRecordItemFilebox : public CXTPReportRecordItem
{
	DECLARE_SERIAL(CMessageRecordItemFilebox)
public:
	// Constructs record item with the initial checkbox value.
	CMessageRecordItemFilebox(LPCTSTR szText = _T(""));

	virtual void OnInplaceButtonDown(CXTPReportInplaceButton* pButton);
	virtual void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText);

	CString GetCaption(CXTPReportColumn* pColumn);
	void SetValue(LPCTSTR szText);
	CString GetValue();
	virtual void DoPropExchange(CXTPPropExchange* pPX);

protected:
	CString m_strText;  // Item text value.
};

AFX_INLINE CString CMessageRecordItemFilebox::GetValue() {
	return m_strText;
}

AFX_INLINE void CMessageRecordItemFilebox::SetValue(LPCTSTR szText){
	m_strText = szText;
}



//////////////////////////////////////////////////////////////////////////
// Enumerates possible Message Importance values for using by 
// CMessageRecordItemImportance class
typedef enum MESSAGE_IMPORTANCE
{
	msgImportanceNormal,
	msgImportanceHigh,
	msgImportanceLow
};


//////////////////////////////////////////////////////////////////////////
// This class is your main custom Record class which you'll manipulate with.
// It contains any kind of specific methods like different types of constructors,
// any additional custom data as class members, any data manipulation methods.
class CMessageRecord : public CXTPReportRecord
{
	DECLARE_SERIAL(CMessageRecord)
public:
	
	// Construct record object using empty values on each field
	CMessageRecord();
	
	// Construct record object from detailed values on each field
	CMessageRecord(
		BOOL bLocal,
		int nProgID,
		CString strName,
		CString strVersion,
//		CString strNewVersion,
		CString strLanguage,
		CString strTLA
//		int nType
		);

	int m_nProgID;

	// Clean up internal objects
	virtual ~CMessageRecord();

	// Create record fields using empty values
	virtual void CreateItems();

	// Set message as read
	BOOL SetRead();

	// Overridden callback method, where we can customize any drawing item metrics.
	virtual void GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics);

	virtual void DoPropExchange(CXTPPropExchange* pPX);
};

//////////////////////////////////////////////////////////////////////////
class CKeyRecord : public CXTPReportRecord
{
	DECLARE_SERIAL(CKeyRecord)
public:
	
	// Construct record object using empty values on each field
	CKeyRecord();
	
	// Construct record object from detailed values on each field
	CKeyRecord(
		CString csFilename,
		int nType
		);

	// Clean up internal objects
	virtual ~CKeyRecord();

	// Create record fields using empty values
	virtual void CreateItems();

	// Set message as read
	BOOL SetRead();
};

#endif // !defined(AFX_MESSAGERECORD_H__A08F955C_1EA1_40B4_A18F_D2B7857FB244__INCLUDED_)
