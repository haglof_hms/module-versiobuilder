//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SampleSuite.rc
//
#define IDR_PROGRAMS                    800
#define IDD_PROGRAMS                    800
#define IDR_SETTINGS                    801
#define IDR_VARIABLES                   802
#define IDD_VARIABLES_FORM1             803
#define IDD_VARIABLES_FORM2             804
#define IDC_PROPERTY_GRID1              7000
#define IDC_PROPERTY_GRID2              7001
#define IDC_TABCONTROL                  7005
#define IDC_PROPERTY_GRID               7006
#define IDR_TOOLBAR_VERSIO              7010
#define IDR_MENU1                       7037
#define IDD_LIST                        7039
#define IDC_TREE                        7059
#define IDC_PROPERTIES                  7061
#define IDC_LIST_HEADER1                7063
#define IDC_PLACEHOLDER1                7068
#define IDC_PLACEHOLDER2                7070
#define IDC_LIST_HEADER2                7071
#define IDC_LIST1                       7072
#define IDC_BUTTON1                     7073
#define IDC_BUTTON2                     7074
#define IDC_COMBO1                      7075
#define ID_BUTTON_INFO                  32771
#define ID_BUTTON_ADD                   32772
#define ID_BUTTON_DEL                   32784
#define ID_ABC_123                      32788
#define ID_ABC_ASDF                     32789
#define ID_ABC_PASTE                    32790
#define ID_INSERT_MENUITEM              32792
#define ID_INSERT_QUESTION              32793
#define ID_INSERT_FUNCTION              32794
#define ID_INSERT_DATA                  32795
#define ID_BUTTON_UP                    32797
#define ID_BUTTON_DOWN                  32798
#define ID_BUTTON_SEND                  32799

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7040
#define _APS_NEXT_COMMAND_VALUE         32800
#define _APS_NEXT_CONTROL_VALUE         7076
#define _APS_NEXT_SYMED_VALUE           7007
#endif
#endif
