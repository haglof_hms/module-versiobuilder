// SampleSuite.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "ProgramsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines


std::vector<HINSTANCE> m_vecHInstTable;
RLFReader* g_pXML;
static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;
static AFX_EXTENSION_MODULE SampleSuiteDLL = { NULL, NULL };

CSampleSuite::CSampleSuite()
{
}

CSampleSuite::~CSampleSuite()
{
}

BOOL CSampleSuite::CheckLicense()
{
	TCHAR szBuf[MAX_PATH], szFilename[MAX_PATH];
	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szFilename, NULL);

	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H2101"), g_pXML->str(800), _T("0"), &szFilename);
	int nRet = AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	if(_tcscmp(msg.getFunc(), _T("1")) != 0 ||
		_tcscmp(msg.getArgStr(), _T("1")) == 0 ||
		_tcscmp(msg.getFunc(), _T("CheckLicense")) == 0)	// demo or invalid by other reasons
	{
		return FALSE;
	}

	return TRUE;
}

CSampleSuite theApp;

#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	TRACE0("DllMain");

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Versio.DLL Initializing!\n");
	
		// create the XML-parser class.
		g_pXML = new RLFReader();

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(SampleSuiteDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Versio.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(SampleSuiteDLL);

		delete g_pXML;
	}

	hInst = hInstance;

	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(SampleSuiteDLL);

	CString csLangFN;
	CString csVersion;
	CString csCopyright;
	CString csCompany;

	csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	app->AddDocTemplate(new CMultiDocTemplate(IDR_PROGRAMS, 
			RUNTIME_CLASS(CProgramsDoc),
			RUNTIME_CLASS(CProgramsFrame),
			RUNTIME_CLASS(CProgramsDlg)));
	idx.push_back(INDEX_TABLE(IDR_PROGRAMS, suite, csLangFN, FALSE));

	csVersion	= getVersionInfo(hInst, VER_NUMBER);
	csCopyright	= getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany	= getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		(csLangFN),
		(csVersion),
		(csCopyright),
		(csCompany)));
}


void DLL_BUILD OpenSuite(int idx, LPCSTR func, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sLangFN;
	CString sVecIndexTableModuleFN;
	CString sCaption;
	int nTableIndex;
	BOOL bFound = FALSE, bIsOneInst = FALSE;
	CString csPath;

	CWinApp* pApp = AfxGetApp();
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() > 0)
	{
		for (UINT i = 0;i < vecIndex.size();i++)
		{
			nTableIndex = vecIndex[i].nTableIndex;
			sVecIndexTableModuleFN = vecIndex[i].szSuite;

			if (nTableIndex == idx && sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			{
				// Get the stringtable resource, matching the TableIndex
				// This string is compared to the title of the document; 051212 p�d
				sResStr.LoadString(nTableIndex);

				// Get filename including searchpath to THIS SUITE, as set in
				// the table index vector, for suites and module(s); 051213 p�d
				sVecIndexTableModuleFN = vecIndex[i].szSuite;

				// Need to setup the Actual filename here, because we need to 
				// get the Language set in registry, on Openning a View; 051214 p�d
				sLangFN = vecIndex[i].szLanguageFN;

				bIsOneInst = vecIndex[i].bOneInstance;

				if (g_pXML->Load(sLangFN))
				{
					sCaption = g_pXML->str(nTableIndex);
				}
				else
				{
					AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
				}

				// Check if the document or module is in this SUITE; 051213 p�d
				if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				{
					POSITION pos = pApp->GetFirstDocTemplatePosition();
					while(pos != NULL)
					{
						pTemplate = pApp->GetNextDocTemplate(pos);
						pTemplate->GetDocString(sDocName, CDocTemplate::docName);
						ASSERT(pTemplate != NULL);
						// Need to add a linefeed, infront of the docName.
						// This is because, for some reason, the document title,
						// set in resource, must have a linefeed.
						// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
						sDocName = '\n' + sDocName;

						if (pTemplate && sDocName.Compare(sResStr) == 0)
						{
							if(bIsOneInst == TRUE)
							{
								// Find the CDocument for this tamplate, and set title.
								// Title is set in Languagefile; OBS! The nTableIndex
								// matches the string id in the languagefile; 051129 p�d
								POSITION posDOC = pTemplate->GetFirstDocPosition();
								while(posDOC != NULL)
								{
									CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
									POSITION pos = pDocument->GetFirstViewPosition();
									if(pos != NULL)
									{
										CView* pView = pDocument->GetNextView(pos);
										pView->GetParent()->BringWindowToTop();
										pView->GetParent()->SetFocus();
										posDOC = (POSITION)1;
										break;
									}
								}

								if(posDOC == NULL)
								{
									CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
									if(pDocument == NULL) break;

									CString sDocTitle;
									sDocTitle.Format(_T("%S"), sCaption);
									pDocument->SetTitle(sDocTitle);
								}
							}
							else
							{
								CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
								if(pDocument == NULL) break;
								
								CString sDocTitle;
								sDocTitle.Format(_T("%S"), sCaption);
								pDocument->SetTitle(sDocTitle);
							}

							break;
						}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
					}	// while(pos != NULL)
				} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				*ret = 1;
			}	// if (nTableIndex == idx)
		}	// for (UINT i = 0;i < vecIndex.size();i++)
	}	// if (vecIndex.size() > 0)
	else
	{
		*ret = 0;
	}
}
