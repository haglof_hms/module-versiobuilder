DP DME Postex
	Val av utdata?
		Dist, Angle360, Angle400, x, y, d1, d2, d3

----------------

Ber�kningstyp
	Parametrar
		M�ste spara ned index p� variablerna?

----------------
L�gga till en Ja/Nej (boolean) variabel

Begr�nsa inmatningsl�ngden f�r caption p� variabler till 5



----------------------

L�gga till inst�llning f�r datumformat
	Kunna v�lja mellan n�gra fasta, typ YYYYMMDD MMDDYYYY osv.
		Implementera detta i klaven ocks�.

-----------------------------------------
Rensar inte countervariabeln?

Kunna v�lja enheter f�r DigiTape? metriskt/english osv.

Se till att alla specialtecken som inte �r till�tna i XML �ndras vid sparning
	T.ex &amp; osv...

�ndra i hanteringen av max vad g�ller decimaltal
	Skriver man max=100 och decimaler=3, s� ska max vara 100,999?
	�ndra s� att max �r ett decimaltal ocks�?
	L�nka max och decimaler, s� att om man �ndrar den ena s� �ndras �ven den andra

*L�gga en "�r du s�ker"-dialog n�r man ska radera variabler

*S�tta gr�ns p� max 50 tecken i textvariabler
	K�ra en max l�ngd i editf�ltet?

*Bort med h�gerklicksmeny

*Cleara h�ger sidan n�r man v�ljer Huvud- och datavaribel rubriken

*Rimlighetskontroller p� variabler?
	Kolla att min �r mindre �n max osv
	Se till att max inte �r st�rre �n vad som g�r

*Krashar n�r man �ndrar namn p� en variabel

*Alltid visa spara dialogen, �ven om det �r en gammal fil.
	*Ta bort *.vbx ur filnamnet

*L�gga in st�d f�r Gator Eyes
	<GatorEyes>1</GatorEyes>

*L�gga in val att kunna ber�kna diametrar med DigiTape

*L�gga till s� att man kan ta in textvariabler via bluetooth

*L�gga till Digitape-variabeltyp

*�ndrar man en programtext, s� raderas namnet f�r den texten.

In med decimaler p� diametrar?
	T.ex 10.5cm
		Detta m�ste implementeras i klave-programmet

*L�gga till postax-variabeltyp

*Har man inte licens, ska man inte kunna v�lja att spara.

-----------------------

INT
	min
	max
	step
	scale

float
	min
	max
	decimals
	scale

text
	maxlength
	scale
	bluetooth

list
	list

date
	scale

scale
	factor

vertex
	factor

digitape
	length/diameter
