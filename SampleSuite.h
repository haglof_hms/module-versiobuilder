#pragma once

#ifndef _SampleSuite_H_
#define _SampleSuite_H_

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

extern HINSTANCE hInst;

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void /*DLL_BUILD*/ InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);


// Open a document view
extern "C" AFX_EXT_API void /*DLL_BUILD*/ OpenSuite(int idx,LPCSTR func,CWnd *,vecINDEX_TABLE &,int *ret);

//extern SampleSuite theApp;
extern RLFReader* g_pXML;

class CSampleSuite //: public CWinApp
{
public:
	CSampleSuite();
	~CSampleSuite();
	BOOL CheckLicense(); // kolla om licensen �r giltig
};

extern CSampleSuite theApp;

#endif
